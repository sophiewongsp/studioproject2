#include "Scene1.h"
#include "GL\glew.h"
#include "Mtx44.h"
#include "shader.hpp"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"

Mtx44 MVP;
  
Scene1::Scene1()
{
}

Scene1::~Scene1()
{
}

void Scene1::Init()
{
	//PlaySound(NULL, 0, 0);
	srand(static_cast<unsigned int>(time(NULL)));

	if (mapNumber == 1)
	{
		numberOfWalls = 46;

		WallBoxes[0].Pt[0] = Vector3(20.1, 0, 42.7);
		WallBoxes[0].Pt[1] = Vector3(22.0, 0, 19.1);
		WallBoxes[0].Pt[2] = Vector3(21.1, 0, 42.7);
		WallBoxes[0].Pt[3] = Vector3(23.0, 0, 19.1);
									
		WallBoxes[1].Pt[0] = Vector3(22.0, 0, 19.1);
		WallBoxes[1].Pt[1] = Vector3(17.7, 0, -72.2);
		WallBoxes[1].Pt[2] = Vector3(23.0, 0, 19.1);
		WallBoxes[1].Pt[3] = Vector3(18.7, 0, -72.2);
									
		WallBoxes[2].Pt[0] = Vector3(17.7, 0, -72.2);
		WallBoxes[2].Pt[1] = Vector3(5.3, 0, -96.9);
		WallBoxes[2].Pt[2] = Vector3(18.7, 0, -73.2);
		WallBoxes[2].Pt[3] = Vector3(6.3, 0, -97.9);
									
		WallBoxes[3].Pt[0] = Vector3(5.3, 0, -96.9);
		WallBoxes[3].Pt[1] = Vector3(-17.5, 0, -110.9);
		WallBoxes[3].Pt[2] = Vector3(6.3, 0, -97.9);
		WallBoxes[3].Pt[3] = Vector3(-16.5, 0, -111.9);
									
		WallBoxes[4].Pt[0] = Vector3(-17.5, 0, -110.9);
		WallBoxes[4].Pt[1] = Vector3(-56.6, 0, -113.5);
		WallBoxes[4].Pt[2] = Vector3(-17.5, 0, -111.9);
		WallBoxes[4].Pt[3] = Vector3(-56.6, 0, -114.5);
									
		WallBoxes[5].Pt[0] = Vector3(-56.6, 0, -113.5);
		WallBoxes[5].Pt[1] = Vector3(-72.5, 0, -111.8);
		WallBoxes[5].Pt[2] = Vector3(-56.6, 0, -114.5);
		WallBoxes[5].Pt[3] = Vector3(-72.5, 0, -112.8);
									
		WallBoxes[6].Pt[0] = Vector3(-72.5, 0, -111.8);
		WallBoxes[6].Pt[1] = Vector3(-88.4, 0, -106.9);
		WallBoxes[6].Pt[2] = Vector3(-73.5, 0, -112.8);
		WallBoxes[6].Pt[3] = Vector3(-89.4, 0, -107.9);
									
		WallBoxes[7].Pt[0] = Vector3(-88.4, 0, -106.9);
		WallBoxes[7].Pt[1] = Vector3(-102.3, 0, -93.7);
		WallBoxes[7].Pt[2] = Vector3(-89.4, 0, -107.9);
		WallBoxes[7].Pt[3] = Vector3(-103.3, 0, -94.7);

		WallBoxes[8].Pt[0] = Vector3(-102.3, 0, -93.7);
		WallBoxes[8].Pt[1] = Vector3(-108.7, 0, -75.3);
		WallBoxes[8].Pt[2] = Vector3(-103.3, 0, -94.7);
		WallBoxes[8].Pt[3] = Vector3(-109.7, 0, -76.3);

		WallBoxes[9].Pt[0] = Vector3(-108.7, 0, -75.3);
		WallBoxes[9].Pt[1] = Vector3(-112.0, 0, -39.2);
		WallBoxes[9].Pt[2] = Vector3(-109.7, 0, -75.3);
		WallBoxes[9].Pt[3] = Vector3(-113.0, 0, -39.2);

		WallBoxes[10].Pt[0] = Vector3(-112.0, 0, -39.2);
		WallBoxes[10].Pt[1] = Vector3(-137.7, 0, -24.4);
		WallBoxes[10].Pt[2] = Vector3(-113.0, 0, -39.7);
		WallBoxes[10].Pt[3] = Vector3(-138.7, 0, -24.9);

		WallBoxes[11].Pt[0] = Vector3(-137.7, 0, -24.4);
		WallBoxes[11].Pt[1] = Vector3(-153.8, 0, -13.3);
		WallBoxes[11].Pt[2] = Vector3(-138.7, 0, -25.4);
		WallBoxes[11].Pt[3] = Vector3(-154.8, 0, -14.3);

		WallBoxes[12].Pt[0] = Vector3(-153.8, 0, -13.3);
		WallBoxes[12].Pt[1] = Vector3(-162.2, 0, 5.49);
		WallBoxes[12].Pt[2] = Vector3(-154.8, 0, -14.3);
		WallBoxes[12].Pt[3] = Vector3(-163.2, 0, 4.49);

		WallBoxes[13].Pt[0] = Vector3(-162.2, 0, 5.49);
		WallBoxes[13].Pt[1] = Vector3(-163.0, 0, 122.4);
		WallBoxes[13].Pt[2] = Vector3(-163.2, 0, 5.49);
		WallBoxes[13].Pt[3] = Vector3(-164.0, 0, 122.4);

		WallBoxes[14].Pt[0] = Vector3(-163.0, 0, 122.4);
		WallBoxes[14].Pt[1] = Vector3(-156.3, 0, 142.6);
		WallBoxes[14].Pt[2] = Vector3(-164.0, 0, 122.9);
		WallBoxes[14].Pt[3] = Vector3(-157.3, 0, 143.1);

		WallBoxes[15].Pt[0] = Vector3(-156.3, 0, 142.6);
		WallBoxes[15].Pt[1] = Vector3(-142.2, 0, 157.5);
		WallBoxes[15].Pt[2] = Vector3(-157.3, 0, 143.6);
		WallBoxes[15].Pt[3] = Vector3(-143.2, 0, 158.5);

		WallBoxes[16].Pt[0] = Vector3(-142.2, 0, 157.5);
		WallBoxes[16].Pt[1] = Vector3(-124.8, 0, 165.7);
		WallBoxes[16].Pt[2] = Vector3(-142.7, 0, 158.5);
		WallBoxes[16].Pt[3] = Vector3(-125.2, 0, 166.7);

		WallBoxes[17].Pt[0] = Vector3(-124.8, 0, 165.7);
		WallBoxes[17].Pt[1] = Vector3(-56.6, 0, 169.6);
		WallBoxes[17].Pt[2] = Vector3(-124.8, 0, 166.7);
		WallBoxes[17].Pt[3] = Vector3(-56.6, 0, 170.6);

		WallBoxes[18].Pt[0] = Vector3(-56.6, 0, 169.6);
		WallBoxes[18].Pt[1] = Vector3(-42.5, 0, 166.5);
		WallBoxes[18].Pt[2] = Vector3(-56.1, 0, 170.6);
		WallBoxes[18].Pt[3] = Vector3(-42.0, 0, 167.5);

		WallBoxes[19].Pt[0] = Vector3(-42.5, 0, 166.5);
		WallBoxes[19].Pt[1] = Vector3(-31.5, 0, 159.3);
		WallBoxes[19].Pt[2] = Vector3(-41.5, 0, 167.0);
		WallBoxes[19].Pt[3] = Vector3(-30.5, 0, 159.8);

		WallBoxes[20].Pt[0] = Vector3(-31.5, 0, 159.3);
		WallBoxes[20].Pt[1] = Vector3(-20.8, 0, 145.4);
		WallBoxes[20].Pt[2] = Vector3(-30.5, 0, 159.3);
		WallBoxes[20].Pt[3] = Vector3(-19.8, 0, 145.4);

		WallBoxes[21].Pt[0] = Vector3(-20.8, 0, 145.4);
		WallBoxes[21].Pt[1] = Vector3(-15.8, 0, 97.9);
		WallBoxes[21].Pt[2] = Vector3(-19.8, 0, 145.4);
		WallBoxes[21].Pt[3] = Vector3(-14.8, 0, 97.9);

		WallBoxes[22].Pt[0] = Vector3(-15.8, 0, 97.9);
		WallBoxes[22].Pt[1] = Vector3(-10.9, 0, 88.7);
		WallBoxes[22].Pt[2] = Vector3(-15.8, 0, 98.4);
		WallBoxes[22].Pt[3] = Vector3(-9.9, 0, 89.2);

		WallBoxes[23].Pt[0] = Vector3(-10.9, 0, 88.7);
		WallBoxes[23].Pt[1] = Vector3(11.7, 0, 67.0);
		WallBoxes[23].Pt[2] = Vector3(-9.9, 0, 89.7);
		WallBoxes[23].Pt[3] = Vector3(12.7, 0, 68.0);

		WallBoxes[24].Pt[0] = Vector3(11.7, 0, 67.0);
		WallBoxes[24].Pt[1] = Vector3(20.1, 0, 42.7);
		WallBoxes[24].Pt[2] = Vector3(12.7, 0, 67.5);
		WallBoxes[24].Pt[3] = Vector3(21.1, 0, 43.2);
		
		//inner
		WallBoxes[25].Pt[0] = Vector3(-15.3, 0, 39.3);
		WallBoxes[25].Pt[1] = Vector3(-15.3, 0, -26.2);
		WallBoxes[25].Pt[2] = Vector3(-16.3, 0, 39.3);
		WallBoxes[25].Pt[3] = Vector3(-16.3, 0, -26.2);

		WallBoxes[26].Pt[0] = Vector3(-15.3, 0, -26.2);
		WallBoxes[26].Pt[1] = Vector3(-17.1, 0, -44.2);
		WallBoxes[26].Pt[2] = Vector3(-16.3, 0, -25.7);
		WallBoxes[26].Pt[3] = Vector3(-18.1, 0, -43.7);

		WallBoxes[27].Pt[0] = Vector3(-17.1, 0, -44.2);
		WallBoxes[27].Pt[1] = Vector3(-21.7, 0, -55.4);
		WallBoxes[27].Pt[2] = Vector3(-18.1, 0, -43.2);
		WallBoxes[27].Pt[3] = Vector3(-22.7, 0, -54.4);

		WallBoxes[28].Pt[0] = Vector3(-21.7, 0, -55.4);
		WallBoxes[28].Pt[1] = Vector3(-31.3, 0, -61.6);
		WallBoxes[28].Pt[2] = Vector3(-21.2, 0, -54.4);
		WallBoxes[28].Pt[3] = Vector3(-30.8, 0, -60.6);

		WallBoxes[29].Pt[0] = Vector3(-31.3, 0, -61.6);
		WallBoxes[29].Pt[1] = Vector3(-70.8, 0, -61.6);
		WallBoxes[29].Pt[2] = Vector3(-31.3, 0, -60.6);
		WallBoxes[29].Pt[3] = Vector3(-70.8, 0, -60.6);

		WallBoxes[30].Pt[0] = Vector3(-70.8, 0, -61.6);
		WallBoxes[30].Pt[1] = Vector3(-81.7, 0, -57.2);
		WallBoxes[30].Pt[2] = Vector3(-69.8, 0, -60.6);
		WallBoxes[30].Pt[3] = Vector3(-80.7, 0, -56.2);

		WallBoxes[31].Pt[0] = Vector3(-81.7, 0, -57.2);
		WallBoxes[31].Pt[1] = Vector3(-84.9, 0, -24.7);
		WallBoxes[31].Pt[2] = Vector3(-80.7, 0, -57.2);
		WallBoxes[31].Pt[3] = Vector3(-83.9, 0, -24.7);

		WallBoxes[32].Pt[0] = Vector3(-84.9, 0, -24.7);
		WallBoxes[32].Pt[1] = Vector3(-92.3, 0, -9.1);
		WallBoxes[32].Pt[2] = Vector3(-83.9, 0, -24.2);
		WallBoxes[32].Pt[3] = Vector3(-91.3, 0, -8.6);

		WallBoxes[33].Pt[0] = Vector3(-92.3, 0, -9.1);
		WallBoxes[33].Pt[1] = Vector3(-104.8, 0, 0.359);
		WallBoxes[33].Pt[2] = Vector3(-91.3, 0, -8.1);
		WallBoxes[33].Pt[3] = Vector3(-103.8, 0, 1.359);

		WallBoxes[34].Pt[0] = Vector3(-104.8, 0, 0.359);
		WallBoxes[34].Pt[1] = Vector3(-122.6, 0, 8.09);
		WallBoxes[34].Pt[2] = Vector3(-103.8, 0, 1.359);
		WallBoxes[34].Pt[3] = Vector3(-121.6, 0, 9.09);

		WallBoxes[35].Pt[0] = Vector3(-122.6, 0, 8.09);
		WallBoxes[35].Pt[1] = Vector3(-125.9, 0, 13.4);
		WallBoxes[35].Pt[2] = Vector3(-121.6, 0, 9.09);
		WallBoxes[35].Pt[3] = Vector3(-124.9, 0, 14.4);

		WallBoxes[36].Pt[0] = Vector3(-125.9, 0, 13.4);
		WallBoxes[36].Pt[1] = Vector3(-125.9, 0, 100.2);
		WallBoxes[36].Pt[2] = Vector3(-124.9, 0, 13.4);
		WallBoxes[36].Pt[3] = Vector3(-124.9, 0, 100.2);

		WallBoxes[37].Pt[0] = Vector3(-125.9, 0, 100.2);
		WallBoxes[37].Pt[1] = Vector3(-122.9, 0, 108.7);
		WallBoxes[37].Pt[2] = Vector3(-124.9, 0, 99.2);
		WallBoxes[37].Pt[3] = Vector3(-121.9, 0, 107.7);

		WallBoxes[38].Pt[0] = Vector3(-122.9, 0, 108.7);
		WallBoxes[38].Pt[1] = Vector3(-117.6, 0, 113.8);
		WallBoxes[38].Pt[2] = Vector3(-121.9, 0, 107.7);
		WallBoxes[38].Pt[3] = Vector3(-116.6, 0, 112.8);

		WallBoxes[39].Pt[0] = Vector3(-117.6, 0, 113.8);
		WallBoxes[39].Pt[1] = Vector3(-97.7, 0, 118.8);
		WallBoxes[39].Pt[2] = Vector3(-117.1, 0, 112.8);
		WallBoxes[39].Pt[3] = Vector3(-97.2, 0, 117.8);

		WallBoxes[40].Pt[0] = Vector3(-97.7, 0, 118.8);
		WallBoxes[40].Pt[1] = Vector3(-54.3, 0, 118.8);
		WallBoxes[40].Pt[2] = Vector3(-97.7, 0, 117.8);
		WallBoxes[40].Pt[3] = Vector3(-54.3, 0, 117.8);

		WallBoxes[41].Pt[0] = Vector3(-54.3, 0, 118.8);
		WallBoxes[41].Pt[1] = Vector3(-46.8, 0, 111.0);
		WallBoxes[41].Pt[2] = Vector3(-55.3, 0, 117.8);
		WallBoxes[41].Pt[3] = Vector3(-47.8, 0, 110.0);

		WallBoxes[42].Pt[0] = Vector3(-46.8, 0, 111.0);
		WallBoxes[42].Pt[1] = Vector3(-44.3, 0, 80.3);
		WallBoxes[42].Pt[2] = Vector3(-47.8, 0, 111.0);
		WallBoxes[42].Pt[3] = Vector3(-45.3, 0, 80.3);

		WallBoxes[43].Pt[0] = Vector3(-44.3, 0, 80.3);
		WallBoxes[43].Pt[1] = Vector3(-37.5, 0, 63.4);
		WallBoxes[43].Pt[2] = Vector3(-45.3, 0, 79.3);
		WallBoxes[43].Pt[3] = Vector3(-38.5, 0, 62.4);

		WallBoxes[44].Pt[0] = Vector3(-37.5, 0, 63.4);
		WallBoxes[44].Pt[1] = Vector3(-20.1, 0, 47.1);
		WallBoxes[44].Pt[2] = Vector3(-38.5, 0, 62.4);
		WallBoxes[44].Pt[3] = Vector3(-21.1, 0, 46.1);

		WallBoxes[45].Pt[0] = Vector3(-20.1, 0, 47.1);
		WallBoxes[45].Pt[1] = Vector3(-15.3, 0, 39.3);
		WallBoxes[45].Pt[2] = Vector3(-21.1, 0, 46.1);
		WallBoxes[45].Pt[3] = Vector3(-16.3, 0, 38.3);
		
		/*
		map1 inner
		-15.3,39.3
		-15.3,-26.2
		-17.1,-44.2
		-21.7,-55.4
		-31.3,-61.6
		-70.8,-61.6
		-81.7,-57.2
		-84.9,-24.7
		-92.3,-9.1
		-104.8,0.359
		-122.6,8.09
		-125.9,13.4
		-125.9,100.2
		-122.9,108.7
		-117.6,113.8
		-97.7,118.8
		-54.3,118.8
		-46.8,111.0
		-44.3,80.3
		-37.5,63.4
		-20.1,47.1

		
		map1 outer
		
		20.1,42.7
		22.0,19.1
		17.7,-72.2
		5.3,-96.9
		-17.5,-110.9
		-56.6,-113.5
		-72.5,-111.8
		-88.4,-106.9
		-102.3,-93.7
		-108.7,-75.3
		-112.0,-39.2
		-137.7,-24.4
		-153.8,-13.3
		-162.2,5.49
		-163.0,122.4
		-156.3,142.6
		-142.2,157.5
		-124.8,165.7
		-56.6,169.6
		-42.5,166.5
		-31.5,159.3
		-20.8,145.4
		-15.8,97.9
		-10.9,88.7
		11.7,67.0
		*/
	}
	else if (mapNumber == 2)
	{
		numberOfWalls = 54;

		WallBoxes[0].Pt[0] = Vector3(-296.4, 0, -178.1);
		WallBoxes[0].Pt[1] = Vector3(-296.4, 0, -127.1);
		WallBoxes[0].Pt[2] = Vector3(-295, 0, -178.1);
		WallBoxes[0].Pt[3] = Vector3(-295, 0, -127.1);
									 
		WallBoxes[1].Pt[0] = Vector3(-296.4, 0, -127.1);
		WallBoxes[1].Pt[1] = Vector3(-278.8, 0, -115.7);
		WallBoxes[1].Pt[2] = Vector3(-295.4, 0, -128.1);
		WallBoxes[1].Pt[3] = Vector3(-277.8, 0, -116.7);
									
		WallBoxes[2].Pt[0] = Vector3(-278.8, 0, -115.65);
		WallBoxes[2].Pt[1] = Vector3(-132.9, 0, -115.65);
		WallBoxes[2].Pt[2] = Vector3(-278.8, 0, -116);
		WallBoxes[2].Pt[3] = Vector3(-132.9, 0, -116);
									
		WallBoxes[3].Pt[0] = Vector3(-132.9, 0, -115.6);
		WallBoxes[3].Pt[1] = Vector3(-88.9, 0, -110.3);
		WallBoxes[3].Pt[2] = Vector3(-132.9, 0, -116.6);
		WallBoxes[3].Pt[3] = Vector3(-88.9, 0, -111.3);
									
		WallBoxes[4].Pt[0] = Vector3(-88.9, 0, -110.3);
		WallBoxes[4].Pt[1] = Vector3(-55.3, 0, -94.4);
		WallBoxes[4].Pt[2] = Vector3(-87.9, 0, -111.3);
		WallBoxes[4].Pt[3] = Vector3(-54.3, 0, -95.4);
									 
		WallBoxes[5].Pt[0] = Vector3(-55.3, 0, -94.4);
		WallBoxes[5].Pt[1] = Vector3(-45.4, 0, -71.0);
		WallBoxes[5].Pt[2] = Vector3(-54.3, 0, -95.4);
		WallBoxes[5].Pt[3] = Vector3(-44.4, 0, -72.0);
						
		WallBoxes[6].Pt[0] = Vector3(-45.4, 0, -71.0);
		WallBoxes[6].Pt[1] = Vector3(-45.4, 0, 144.8);
		WallBoxes[6].Pt[2] = Vector3(-44.4, 0, -71.0);
		WallBoxes[6].Pt[3] = Vector3(-44.4, 0, 144.8);
									 
		WallBoxes[7].Pt[0] = Vector3(-45.4, 0, 144.8);
		WallBoxes[7].Pt[1] = Vector3(-42.4, 0, 161.7);
		WallBoxes[7].Pt[2] = Vector3(-44.4, 0, 143.8);
		WallBoxes[7].Pt[3] = Vector3(-41.4, 0, 160.7);
		
		WallBoxes[8].Pt[0] = Vector3(-42.4, 0, 161.7);
		WallBoxes[8].Pt[1] = Vector3(-36.3, 0, 170.9);
		WallBoxes[8].Pt[2] = Vector3(-41.4, 0, 160.7);
		WallBoxes[8].Pt[3] = Vector3(-35.3, 0, 169.9);

		WallBoxes[9].Pt[0] = Vector3(-36.3, 0, 170.9);
		WallBoxes[9].Pt[1] = Vector3(45.7, 0, 170.9);
		WallBoxes[9].Pt[2] = Vector3(-36.3, 0, 169.9);
		WallBoxes[9].Pt[3] = Vector3(45.7, 0, 169.9);
		
		WallBoxes[10].Pt[0] = Vector3(45.7, 0, 170.9);
		WallBoxes[10].Pt[1] = Vector3(59.3, 0, 164.4);
		WallBoxes[10].Pt[2] = Vector3(44.7, 0, 169.9);
		WallBoxes[10].Pt[3] = Vector3(58.3, 0, 163.4);

		WallBoxes[11].Pt[0] = Vector3(59.3, 0, 164.4);
		WallBoxes[11].Pt[1] = Vector3(61.9, 0, 147.5);
		WallBoxes[11].Pt[2] = Vector3(58.3, 0, 163.4);
		WallBoxes[11].Pt[3] = Vector3(60.9, 0, 146.5);

		WallBoxes[12].Pt[0] = Vector3(62.0, 0, 147.5);
		WallBoxes[12].Pt[1] = Vector3(62.0, 0, -74.9);
		WallBoxes[12].Pt[2] = Vector3(61.0, 0, 147.5);
		WallBoxes[12].Pt[3] = Vector3(61.0, 0, -74.9);

		WallBoxes[13].Pt[0] = Vector3(62.0, 0, -74.9);
		WallBoxes[13].Pt[1] = Vector3(66.6, 0, -87.0);
		WallBoxes[13].Pt[2] = Vector3(61.0, 0, -75.9);
		WallBoxes[13].Pt[3] = Vector3(65.6, 0, -88.0);

		WallBoxes[14].Pt[0] = Vector3(66.6, 0, -87.0);
		WallBoxes[14].Pt[1] = Vector3(77.3, 0, -98.3);
		WallBoxes[14].Pt[2] = Vector3(65.6, 0, -88.0);
		WallBoxes[14].Pt[3] = Vector3(76.3, 0, -99.3);

		WallBoxes[15].Pt[0] = Vector3(77.3, 0, -98.3);
		WallBoxes[15].Pt[1] = Vector3(96.3, 0, -107.1);
		WallBoxes[15].Pt[2] = Vector3(76.3, 0, -99.3);
		WallBoxes[15].Pt[3] = Vector3(95.3, 0, -108.1);

		WallBoxes[16].Pt[0] = Vector3(96.3, 0, -107.1);
		WallBoxes[16].Pt[1] = Vector3(119.5, 0, -114.2);
		WallBoxes[16].Pt[2] = Vector3(95.3, 0, -108.1);
		WallBoxes[16].Pt[3] = Vector3(118.5, 0, -115.2);

		WallBoxes[17].Pt[0] = Vector3(119.5, 0, -114.2);
		WallBoxes[17].Pt[1] = Vector3(178.1, 0, -114.2);
		WallBoxes[17].Pt[2] = Vector3(119.5, 0, -115.2);
		WallBoxes[17].Pt[3] = Vector3(178.1, 0, -115.2);

		WallBoxes[18].Pt[0] = Vector3(178.1, 0, -114.2);
		WallBoxes[18].Pt[1] = Vector3(166.2, 0, -180.4);
		WallBoxes[18].Pt[2] = Vector3(177.1, 0, -113.2);
		WallBoxes[18].Pt[3] = Vector3(165.2, 0, -179.4);

		WallBoxes[19].Pt[0] = Vector3(166.2, 0, -180.4);
		WallBoxes[19].Pt[1] = Vector3(-177.8, 0, -180.4);
		WallBoxes[19].Pt[2] = Vector3(166.2, 0, -179.4);
		WallBoxes[19].Pt[3] = Vector3(-177.8, 0, -179.4);

		WallBoxes[20].Pt[0] = Vector3(-177.8, 0, -180.4);
		WallBoxes[20].Pt[1] = Vector3(-287.4, 0, -189.1);
		WallBoxes[20].Pt[2] = Vector3(-177.8, 0, -179.4);
		WallBoxes[20].Pt[3] = Vector3(-287.4, 0, -188.1);

		WallBoxes[21].Pt[0] = Vector3(-287.4, 0, -189.1);
		WallBoxes[21].Pt[1] = Vector3(-296.4, 0, -178.1);
		WallBoxes[21].Pt[2] = Vector3(-287.4, 0, -188.1);
		WallBoxes[21].Pt[3] = Vector3(-296.4, 0, -177.1);

		//long box
		WallBoxes[22].Pt[0] = Vector3(-128.4, 0, -208.4);
		WallBoxes[22].Pt[1] = Vector3(144.09, 0, -208.4);
		WallBoxes[22].Pt[2] = Vector3(-128.4, 0, -209.4);
		WallBoxes[22].Pt[3] = Vector3(144.09, 0, -209.4);

		WallBoxes[23].Pt[0] = Vector3(112.6, 0, -234.56);
		WallBoxes[23].Pt[1] = Vector3(-73.475, 0, -234.56);
		WallBoxes[23].Pt[2] = Vector3(112.6, 0, -233.56);
		WallBoxes[23].Pt[3] = Vector3(-73.475, 0, -233.56);

		WallBoxes[24].Pt[0] = Vector3(-128.4, 0, -208.29);
		WallBoxes[24].Pt[1] = Vector3(-73.47, 0, -234.56);
		WallBoxes[24].Pt[2] = Vector3(-127.4, 0, -207.29);
		WallBoxes[24].Pt[3] = Vector3(-72.47, 0, -233.56);

		WallBoxes[25].Pt[0] = Vector3(144.09, 0, -208.7);
		WallBoxes[25].Pt[1] = Vector3(112.6, 0, -234.56);
		WallBoxes[25].Pt[2] = Vector3(143.09, 0, -207.7);
		WallBoxes[25].Pt[3] = Vector3(111.6, 0, -233.56);

		// top box
		WallBoxes[26].Pt[0] = Vector3(212.4, 0, -182.6);
		WallBoxes[26].Pt[2] = Vector3(212.4, 0, -113.06);
		WallBoxes[26].Pt[1] = Vector3(213.4, 0, -182.6);
		WallBoxes[26].Pt[3] = Vector3(213.4, 0, -113.06);
		
		WallBoxes[27].Pt[0] = Vector3(287.4, 0, -113.6);
		WallBoxes[27].Pt[2] = Vector3(212.4, 0, -113.06);
		WallBoxes[27].Pt[1] = Vector3(287.4, 0, -114.6);
		WallBoxes[27].Pt[3] = Vector3(213.4, 0, -114.06);

		WallBoxes[28].Pt[0] = Vector3(287.4, 0, -113.6);
		WallBoxes[28].Pt[2] = Vector3(287.4, 0, -183.0);
		WallBoxes[28].Pt[1] = Vector3(286.4, 0, -114.6);
		WallBoxes[28].Pt[3] = Vector3(286.4, 0, -183.0);

		WallBoxes[29].Pt[0] = Vector3(287.4, 0, -183.0);
		WallBoxes[29].Pt[2] = Vector3(212.4, 0, -182.6);
		WallBoxes[29].Pt[1] = Vector3(287.4, 0, -182.0);
		WallBoxes[29].Pt[3] = Vector3(212.4, 0, -181.6);
		
		//outer
		WallBoxes[30].Pt[0] = Vector3(-273.9, 0, -220.3);
		WallBoxes[30].Pt[2] = Vector3(-300.4, 0, -216.2);
		WallBoxes[30].Pt[1] = Vector3(-274.9, 0, -221.3);
		WallBoxes[30].Pt[3] = Vector3(-301.4, 0, -217.2);

		WallBoxes[31].Pt[0] = Vector3(-300.4, 0, -216.2);
		WallBoxes[31].Pt[2] = Vector3(-321.8, 0, -208.1);
		WallBoxes[31].Pt[1] = Vector3(-301.4, 0, -217.2);
		WallBoxes[31].Pt[3] = Vector3(-322.8, 0, -209.1);

		WallBoxes[32].Pt[0] = Vector3(-321.8, 0, -208.1);
		WallBoxes[32].Pt[2] = Vector3(-342.1, 0, -179.2);
		WallBoxes[32].Pt[1] = Vector3(-322.8, 0, -209.1);
		WallBoxes[32].Pt[3] = Vector3(-343.1, 0, -180.2);

		WallBoxes[33].Pt[0] = Vector3(-342.1, 0, -179.2);
		WallBoxes[33].Pt[2] = Vector3(-342.1, 0, -127.4);
		WallBoxes[33].Pt[1] = Vector3(-343.1, 0, -179.2);
		WallBoxes[33].Pt[3] = Vector3(-343.1, 0, -127.4);

		WallBoxes[34].Pt[0] = Vector3(-342.1, 0, -127.4);
		WallBoxes[34].Pt[2] = Vector3(-329.1, 0, -105.3);
		WallBoxes[34].Pt[1] = Vector3(-343.1, 0, -126.4);
		WallBoxes[34].Pt[3] = Vector3(-330.1, 0, -104.3);

		WallBoxes[35].Pt[0] = Vector3(-329.1, 0, -105.3);
		WallBoxes[35].Pt[2] = Vector3(-296.3, 0, -88.9);
		WallBoxes[35].Pt[1] = Vector3(-330.1, 0, -104.3);
		WallBoxes[35].Pt[3] = Vector3(-297.3, 0, -87.9);

		WallBoxes[36].Pt[0] = Vector3(-296.3, 0, -88.9);
		WallBoxes[36].Pt[2] = Vector3(-98.2, 0, 81.3);
		WallBoxes[36].Pt[1] = Vector3(-296.3, 0, -87.9);
		WallBoxes[36].Pt[3] = Vector3(-98.2, 0, 82.3);

		WallBoxes[37].Pt[0] = Vector3(-91.1, 0, -72.3);
		WallBoxes[37].Pt[2] = Vector3(-91.1, 0, 148.7);
		WallBoxes[37].Pt[1] = Vector3(-90.1, 0, -72.3);
		WallBoxes[37].Pt[3] = Vector3(-90.1, 0, 148.7);

		WallBoxes[38].Pt[0] = Vector3(-91.1, 0, 148.7);
		WallBoxes[38].Pt[2] = Vector3(-78.0, 0, 180.8);
		WallBoxes[38].Pt[1] = Vector3(-92.1, 0, 149.7);
		WallBoxes[38].Pt[3] = Vector3(-79.0, 0, 181.8);

		WallBoxes[39].Pt[0] = Vector3(-78.0, 0, 180.8);
		WallBoxes[39].Pt[2] = Vector3(-56.7, 0, 193.4);
		WallBoxes[39].Pt[1] = Vector3(-79.0, 0, 181.8);
		WallBoxes[39].Pt[3] = Vector3(-57.7, 0, 194.4);

		WallBoxes[40].Pt[0] = Vector3(-56.7, 0, 193.4);
		WallBoxes[40].Pt[2] = Vector3(-15.6, 0, 199.0);
		WallBoxes[40].Pt[1] = Vector3(-57.7, 0, 194.4);
		WallBoxes[40].Pt[3] = Vector3(-16.6, 0, 200.0);

		WallBoxes[41].Pt[0] = Vector3(-15.6, 0, 199.0);
		WallBoxes[41].Pt[2] = Vector3(52.1, 0, 199.0);
		WallBoxes[41].Pt[1] = Vector3(-15.6, 0, 200.0);
		WallBoxes[41].Pt[3] = Vector3(52.1, 0, 200.0);

		WallBoxes[42].Pt[0] = Vector3(52.1, 0, 199.0);
		WallBoxes[42].Pt[2] = Vector3(82.2, 0, 191.0);
		WallBoxes[42].Pt[1] = Vector3(53.1, 0, 200.0);
		WallBoxes[42].Pt[3] = Vector3(83.2, 0, 192.0);

		WallBoxes[43].Pt[0] = Vector3(82.2, 0, 191.0);
		WallBoxes[43].Pt[2] = Vector3(101.7, 0, 172.7);
		WallBoxes[43].Pt[1] = Vector3(83.2, 0, 192.0);
		WallBoxes[43].Pt[3] = Vector3(102.7, 0, 173.7);

		WallBoxes[44].Pt[0] = Vector3(101.7, 0, 172.7);
		WallBoxes[44].Pt[2] = Vector3(107.9, 0, 147.4);
		WallBoxes[44].Pt[1] = Vector3(102.7, 0, 173.7);
		WallBoxes[44].Pt[3] = Vector3(108.9, 0, 148.4);

		WallBoxes[45].Pt[0] = Vector3(107.9, 0, 147.4);
		WallBoxes[45].Pt[2] = Vector3(107.9, 0, -77.2);
		WallBoxes[45].Pt[1] = Vector3(108.9, 0, 147.4);
		WallBoxes[45].Pt[3] = Vector3(108.9, 0, -77.2);

		WallBoxes[46].Pt[0] = Vector3(123.9, 0, -83.9);
		WallBoxes[46].Pt[2] = Vector3(223.0, 0, -83.8);
		WallBoxes[46].Pt[1] = Vector3(123.9, 0, -82.9);
		WallBoxes[46].Pt[3] = Vector3(223.0, 0, -82.8);

		WallBoxes[47].Pt[0] = Vector3(223.0, 0, -83.8);
		WallBoxes[47].Pt[2] = Vector3(292.2, 0, -87.4);
		WallBoxes[47].Pt[1] = Vector3(224.0, 0, -82.8);
		WallBoxes[47].Pt[3] = Vector3(293.2, 0, -86.4);

		WallBoxes[48].Pt[0] = Vector3(292.2, 0, -87.4);
		WallBoxes[48].Pt[2] = Vector3(329.1, 0, -107.7);
		WallBoxes[48].Pt[1] = Vector3(293.2, 0, -86.4);
		WallBoxes[48].Pt[3] = Vector3(330.1, 0, -106.7);

		WallBoxes[49].Pt[0] = Vector3(329.1, 0, -107.7);
		WallBoxes[49].Pt[2] = Vector3(336.5, 0, -173.6);
		WallBoxes[49].Pt[1] = Vector3(330.1, 0, -108.7);
		WallBoxes[49].Pt[3] = Vector3(337.5, 0, -174.6);

		WallBoxes[50].Pt[0] = Vector3(336.5, 0, -173.6);
		WallBoxes[50].Pt[2] = Vector3(332.1, 0, -186.9);
		WallBoxes[50].Pt[1] = Vector3(337.5, 0, -174.6);
		WallBoxes[50].Pt[3] = Vector3(333.1, 0, -187.9);

		WallBoxes[51].Pt[0] = Vector3(332.1, 0, -186.9);
		WallBoxes[51].Pt[2] = Vector3(318.0, 0, -201.1);
		WallBoxes[51].Pt[1] = Vector3(333.1, 0, -187.9);
		WallBoxes[51].Pt[3] = Vector3(319.0, 0, -202.1);

		WallBoxes[52].Pt[0] = Vector3(318.0, 0, -201.1);
		WallBoxes[52].Pt[2] = Vector3(157.1, 0, -260.6);
		WallBoxes[52].Pt[1] = Vector3(319.0, 0, -202.1);
		WallBoxes[52].Pt[3] = Vector3(158.1, 0, -261.6);

		WallBoxes[53].Pt[0] = Vector3(157.1, 0, -260.6);
		WallBoxes[53].Pt[2] = Vector3(-119.0, 0, -261.3);
		WallBoxes[53].Pt[1] = Vector3(157.1, 0, -261.6);
		WallBoxes[53].Pt[3] = Vector3(-119.0, 0, -262.3);
		

		//map inner
		/*
		outer 

		-119.0,-261.3
		-273.9,-220.3
		-300.4,-216.2
		-321.8,-208.1
		-342.1, -179.2
		-342.1,-127.4
		-329.1,-105.3
		-296.3,-88.9
		-98.2,81.3
		next
		-91.1,-72.3
		-91.1,148.7
		-78.0,180.8
		-56.7,193.4
		-15.6,199.0
		52.1,199.0
		82.2,191.0
		101.74,172.7
		107.9,147.4
		107.9,-77.2
		next
		123.9,-83.9
		223.0,-83.8
		292.2,-87.4
		329.1,-107.7
		336.5,-173.6
		332.1,-186.9
		318.0,-201.1
		157.1,-260.6
		-118.7,-261.6


		-296.4, 0, -178.1
		-296.4,-127.1
		-278.8,-115.7
		-132.9,-115.6
		-88.9,-110.3   5
		-55.3,-94.4
		-45.4,-71.0
		-45.4,144.8
		-42.4,161.7
		-36.3,170.9   10
		45.7,170.9
		59.3,164.4
		61.9,147.5
		62.0,-74.9
		66.6,-87.0      15
		77.3,-98.3
		96.328,-107.1
		119.5,-114.2
		178.1,-114.2   
		166.2,-180.4     20
		-177.8,-180.4
		-287.4,-189.1

		long area
		-128.4,-208.29
		144.09,-208.7
		112.6,-234.56
		-73.475,-234.56

		small area
		212.4,-182.6
		212.4,-113.06
		287.4,-113.6  
		287.4,-183.0   31
		*/
	}
	else if (mapNumber == 3)
	{
		numberOfWalls = 8;

		WallBoxes[0].Pt[0] = Vector3(-12, 0, 73);
		WallBoxes[0].Pt[1] = Vector3(-118.5f, 0, 73);
		WallBoxes[0].Pt[2] = Vector3(-12, 0, -135);
		WallBoxes[0].Pt[3] = Vector3(-118.5f, 0, -135);
					 
		WallBoxes[1].Pt[0] = Vector3(-117.5f, 0, 29.5f);
		WallBoxes[1].Pt[1] = Vector3(-222, 0, 29.5f);
		WallBoxes[1].Pt[2] = Vector3(-117.5f, 0, 174);
		WallBoxes[1].Pt[3] = Vector3(-222, 0, 174);
					
		WallBoxes[2].Pt[0] = Vector3(24, 0, 118.5f);
		WallBoxes[2].Pt[1] = Vector3(-73, 0, 118.5f);
		WallBoxes[2].Pt[2] = Vector3(24, 0, 220);
		WallBoxes[2].Pt[3] = Vector3(-73, 0, 220);
					
		WallBoxes[3].Pt[0] = Vector3(24, 0, 118.5f);
		WallBoxes[3].Pt[1] = Vector3(24, 0, -179);
		WallBoxes[3].Pt[2] = Vector3(25, 0, 118.5f);
		WallBoxes[3].Pt[3] = Vector3(25, 0, -179);
					
		WallBoxes[4].Pt[0] = Vector3(24, 0, -179);
		WallBoxes[4].Pt[1] = Vector3(-161, 0, -179);
		WallBoxes[4].Pt[2] = Vector3(24, 0, -180);
		WallBoxes[4].Pt[3] = Vector3(-161, 0, -180);
					
		WallBoxes[5].Pt[0] = Vector3(-269, 0, -16);
		WallBoxes[5].Pt[1] = Vector3(-161, 0, -16);
		WallBoxes[5].Pt[2] = Vector3(-269, 0, -179);
		WallBoxes[5].Pt[3] = Vector3(-161, 0, -179);
					 
		WallBoxes[6].Pt[0] = Vector3(-269, 0, 220);
		WallBoxes[6].Pt[1] = Vector3(-269, 0, -16);
		WallBoxes[6].Pt[2] = Vector3(-270, 0, 220);
		WallBoxes[6].Pt[3] = Vector3(-270, 0, -16);
					
		WallBoxes[7].Pt[0] = Vector3(-73, 0, 220);
		WallBoxes[7].Pt[1] = Vector3(-269, 0, 220);
		WallBoxes[7].Pt[2] = Vector3(-73, 0, 221);
		WallBoxes[7].Pt[3] = Vector3(-269, 0, 221);
	}

	glClearColor(0.5f, 0.5f, 1.f, 0.5f);
	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);
	glEnable(GL_CULL_FACE);

	camera.setTargetDist(Vector3(0, -35, 0));
	player2Camera.setTargetDist(Vector3(0, -35, 0));
	camera.setCameraAngle(-5);
	player2Camera.setCameraAngle(14);

	backCamera.setTargetDist(Vector3(0, -35, 0));
	backCamera2.setTargetDist(Vector3(0, -35, 0));
	backCamera.setCameraAngle(-5);
	backCamera2.setCameraAngle(14);

	camera.Init(Vector3(car.getCposx(), car.getCposy() - 35, car.getCposz() + 40), Vector3(car.getCposx(), car.getCposy() - 5, car.getCposz()), Vector3(0, 1, 0), Vector3(car.getCposx()), Vector3(car.getCtarget()), Vector3(car.getCpos()));
	player2Camera.Init(Vector3(car2.getCposx2(), car2.getCposy2() - 35, car2.getCposz2() + 53), Vector3(car2.getCposx2(), car2.getCposy2() + 14, car2.getCposz2()), Vector3(0, 1, 0), Vector3(car2.getCposx2()), Vector3(car2.getCtarget2()), Vector3(car2.getCpos2()));
	backCamera.Init(Vector3(car.getCposx(), car.getCposy() - 35, car.getCposz() - 40), Vector3(car.getCposx(), car.getCposy() - 5, car.getCposz()), Vector3(0, 1, 0), Vector3(car.getCposx()), Vector3(car.getCtarget()), Vector3(car.getCpos()));
	backCamera2.Init(Vector3(car2.getCposx2(), car2.getCposy2() - 35, car2.getCposz2() - 53), Vector3(car2.getCposx2(), car2.getCposy2() + 14, car2.getCposz2()), Vector3(0, 1, 0), Vector3(car2.getCposx2()), Vector3(car2.getCtarget2()), Vector3(car2.getCpos2()));
	endGame.Init(Vector3(100,100,100),Vector3(0,100,0),Vector3(0,1,0));
	//miniMapCamera.Init(Vector3(200, 1000, car.getCposz()), Vector3(0, 1, car.getCposz()), Vector3(0, 1, 0));
	//miniMapCamera.Init(Vector3(500, 1000, 1000), Vector3(500, 0, 0), Vector3(0, 1, 0));
	//miniMapCamera.Init(Vector3(340, 890, 480), Vector3(340, 0, 0), Vector3(0, 0, 1));

	if (mapNumber == 1)
	{
		car.init(Vector3(15, 0, -10), Vector3(15, 0, -9), Vector3(0, 1, 0));
		car2.init(Vector3(-10, 0, -10), Vector3(-10, 0, -9), Vector3(0, 1, 0));
	}
	if (mapNumber == 2)
	{
		car.init(Vector3(100, 0, -1), Vector3(100, 0, 0), Vector3(0, 1, 0));
		car2.init(Vector3(70, 0, -1), Vector3(70, 0, 0), Vector3(0, 1, 0));
	}
	if (mapNumber == 3)
	{
		car.init(Vector3(15, 0, -10), Vector3(15, 0, -9), Vector3(0, 1, 0));
		car2.init(Vector3(-5, 0, -10), Vector3(-5, 0, -9), Vector3(0, 1, 0));
	}

	//get normals, map 1 and 3
	if (mapNumber == 1 || mapNumber == 3)
	{
		Collide.lapPoints[0] = Vector3(24.5f, 0, -1);
		Collide.lapPoints[1] = Vector3(-13, 0, -1);
		Collide.lapPoints[2] = Vector3(-13, 0, 1);
		Collide.lapPoints[3] = Vector3(24.5f, 0, 1);
		Collide.lapsNormal[0] = (Collide.lapPoints[0] - Collide.lapPoints[1]).Normalized();
		Collide.lapsNormal[1] = (Collide.lapPoints[1] - Collide.lapPoints[2]).Normalized();
	}
	else
	{
		//get normals map 2
		Collide.lapPoints[0] = Vector3(58.f, 0, 41.5f);
		Collide.lapPoints[1] = Vector3(111.f, 0, 41.5f);
		Collide.lapPoints[2] = Vector3(111.f, 0, 44.f);
		Collide.lapPoints[3] = Vector3(58.f, 0, 44.f);
		Collide.lapsNormal[0] = (Collide.lapPoints[0] - Collide.lapPoints[1]).Normalized();
		Collide.lapsNormal[1] = (Collide.lapPoints[1] - Collide.lapPoints[2]).Normalized();
	}



	rotateArm = 0.0f;
	rotateArm1 = 0.0f;

	//coordinate checking(can delete after done)
	cubeZ = cubeX = 0.0f;


	//bool for collision
	for (int i = 0; i < 4; i++)
		Col[i] = false;
	for (int i = 0; i < 4; i++)
		BananaCol[i] = false;
	for (int i = 0; i < 4; i++)
		carToLaps[i] = false;
	for (int i = 0; i < 4; i++)
		car2ToLaps[i] = false;
	bananaPos.SetZero();

	//for laps count
	isTouched = isTouched2 = false;
	car1lap = car2lap = -1;
	delaylapTime = delaylapTime2 = 15.0f;
	lapTime = lapTime2 = 0;

	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 2000.f);
	projectionStack.LoadMatrix(projection);

	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");

	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID,"material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID,"material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID,"material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID,"material.kShininess");
	// light 0
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID,"lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID,"lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");
	
	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");

	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");

	m_parameters[U_LIGHT0_TYPE] = glGetUniformLocation(m_programID, "lights[0].type");
	m_parameters[U_LIGHT0_SPOTDIRECTION] = glGetUniformLocation(m_programID,"lights[0].spotDirection");
	m_parameters[U_LIGHT0_COSCUTOFF] = glGetUniformLocation(m_programID,"lights[0].cosCutoff");
	m_parameters[U_LIGHT0_COSINNER] = glGetUniformLocation(m_programID,"lights[0].cosInner");
	m_parameters[U_LIGHT0_EXPONENT] = glGetUniformLocation(m_programID,"lights[0].exponent");

	// Get a handle for our "textColor" uniform
	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID,"textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID,"textColor");

	// Get a handle for our "colorTexture" uniform
	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID,"colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");

	// directional light
	light[0].type = Light::LIGHT_DIRECTIONAL;
	light[0].position.Set(300, 700, 0);
	light[0].color.Set(1, 1, 1);
	light[0].power = 0.5;
	light[0].kC = 1.f;
	light[0].kL = 0.01f;
	light[0].kQ = 0.001f;
	light[0].cosCutoff = cos(Math::DegreeToRadian(45));
	light[0].cosInner = cos(Math::DegreeToRadian(30));
	light[0].exponent = 3.f;
	light[0].spotDirection.Set(0.f, 1.f, 0.f);

	// Use our shader
	glUseProgram(m_programID);
	// Enable depth test
	glDisable(GL_CULL_FACE);

	// Enable blending (Prac 9 part 2)
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 1000.f, 1000.f, 1000.f);

	// Skybox
	meshList[GEO_FRONT] = MeshBuilder::GenerateQuad2("front", Color(1.f, 1.f, 1.f), 1.f);
	if (mapNumber == 1)
	{
		meshList[GEO_FRONT]->textureID = LoadTGA("Image//front1.tga");
	}
	else if (mapNumber == 2)
	{
		meshList[GEO_FRONT]->textureID = LoadTGA("Image//front2.tga");
	}
	else
	{
		meshList[GEO_FRONT]->textureID = LoadTGA("Image//front3.tga");
	}
	
	meshList[GEO_BACK] = MeshBuilder::GenerateQuad2("back", Color(1.f, 1.f, 1.f), 1.f);
	if (mapNumber == 1)
	{
		meshList[GEO_BACK]->textureID = LoadTGA("Image//back1.tga");
	}
	else if (mapNumber == 2)
	{
		meshList[GEO_BACK]->textureID = LoadTGA("Image//back2.tga");
	}
	else
	{
		meshList[GEO_BACK]->textureID = LoadTGA("Image//back3.tga");
	}

	meshList[GEO_TOP] = MeshBuilder::GenerateQuad2("top", Color(1.f, 1.f, 1.f), 1.f);
	if (mapNumber == 1)
	{
	meshList[GEO_TOP]->textureID = LoadTGA("Image//top1.tga");
	}
	else if (mapNumber == 2)
	{
		meshList[GEO_TOP]->textureID = LoadTGA("Image//top2.tga");
	}
	else
	{
		meshList[GEO_TOP]->textureID = LoadTGA("Image//top3.tga");
	}

	meshList[GEO_BOTTOM] = MeshBuilder::GenerateQuad2("bottom", Color(1.f, 1.f, 1.f), 1.f);
	if (mapNumber == 1)
	{
		meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//bottom1.tga");
	}
	else if (mapNumber == 2)
	{
		meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//bottom2.tga");
	}
	else
	{
		meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//bottom3.tga");
	}

	meshList[GEO_LEFT] = MeshBuilder::GenerateQuad2("left", Color(1.f, 1.f, 1.f), 1.f);
	if (mapNumber == 1)
	{
		meshList[GEO_LEFT]->textureID = LoadTGA("Image//left1.tga");
	}
	else if (mapNumber == 2)
	{
		meshList[GEO_LEFT]->textureID = LoadTGA("Image//left2.tga");
	}
	else
	{
		meshList[GEO_LEFT]->textureID = LoadTGA("Image//left3.tga");
	}

	meshList[GEO_RIGHT] = MeshBuilder::GenerateQuad2("right", Color(1.f, 1.f, 1.f), 1.f);
	if (mapNumber == 1)
	{
		meshList[GEO_RIGHT]->textureID = LoadTGA("Image//right1.tga");
	}
	else if (mapNumber == 2)
	{
		meshList[GEO_RIGHT]->textureID = LoadTGA("Image//right2.tga");
	}
	else
	{
		meshList[GEO_RIGHT]->textureID = LoadTGA("Image//right3.tga");
	}
	
	////map 1
	if (mapNumber == 1)
	{
		//meshList[RACETRACK_FinishLine] = MeshBuilder::GenerateOBJ("Track 1", "OBJ//Race_Track1_FinishingLine.obj");
		//meshList[RACETRACK_FinishLine]->textureID = LoadTGA("Image//RaceTrack_1_FinishingLine.tga");
		meshList[RACETRACK_WallInner] = MeshBuilder::GenerateOBJ("Track 1s", "OBJ//Race_Track1_InnerWall.obj");
		meshList[RACETRACK_WallInner]->textureID = LoadTGA("Image//RaceTrack_1_InnerWall.tga");
		meshList[RACETRACK_WallOuter] = MeshBuilder::GenerateOBJ("Track 1ss", "OBJ//Race_Track1_OuterWall.obj");
		meshList[RACETRACK_WallOuter]->textureID = LoadTGA("Image//RaceTrack_1_OuterWall.tga");
		meshList[RACETRACK_Road] = MeshBuilder::GenerateOBJ("Track 1sss", "OBJ//Race_Track1_Road.obj");
		meshList[RACETRACK_Road]->textureID = LoadTGA("Image//RaceTrack_1_Road.tga");
		meshList[RACETRACK_Road]->material.kAmbient.Set(0.6f, 0.6f, 0.6f);
		meshList[RACETRACK_Road]->material.kDiffuse.Set(0.3f, 0.3f, 0.3f);
		meshList[RACETRACK_Road]->material.kSpecular.Set(0.6f, 0.6f, 0.6f);
		meshList[RACETRACK_Road]->material.kShininess = 1.f;
	}
	else if (mapNumber == 2)//map 2
	{
		meshList[RACETRACK_FinishLine] = MeshBuilder::GenerateOBJ("Track 2", "OBJ//Race_Track2_FinishingLine.obj");
		meshList[RACETRACK_FinishLine]->textureID = LoadTGA("Image//RaceTrack_2_FinishingLine.tga");
		meshList[RACETRACK_Building] = MeshBuilder::GenerateOBJ("Track 2", "OBJ//Race_Track2_Buildings.obj");
		meshList[RACETRACK_Building]->textureID = LoadTGA("Image//RaceTrack_2_Building.tga");
		meshList[RACETRACK_Road] = MeshBuilder::GenerateOBJ("Track 2", "OBJ//Race_Track2_Road.obj");
		meshList[RACETRACK_Road]->textureID = LoadTGA("Image//RaceTrack_2_Road.tga");
	}	
	else //map 3
	{
		meshList[RACETRACK_WallOuter] = MeshBuilder::GenerateOBJ("Track 3", "OBJ//Map3_road.obj");
		meshList[RACETRACK_WallOuter]->textureID = LoadTGA("Image//road3.tga");
		meshList[RACETRACK_Road] = MeshBuilder::GenerateOBJ("Track 3", "OBJ//Map3_wall.obj");
		meshList[RACETRACK_Road]->textureID = LoadTGA("Image//wall3.tga");
	}

	// Models
	meshList[GEO_MODEL1] = MeshBuilder::GenerateOBJ("model1", "OBJ//robot.obj");
	meshList[GEO_MODEL1]->textureID = LoadTGA("Image//robot.tga");
	meshList[GEO_MODEL1]->material.kAmbient.Set(0.6f, 0.6f, 0.6f);
	meshList[GEO_MODEL1]->material.kDiffuse.Set(0.3f, 0.3f, 0.3f);
	meshList[GEO_MODEL1]->material.kSpecular.Set(0.6f, 0.6f, 0.6f);
	meshList[GEO_MODEL1]->material.kShininess = 1.f;

	meshList[GEO_MODEL2] = MeshBuilder::GenerateOBJ("model2", "OBJ//robotArm1.obj");
	meshList[GEO_MODEL2]->textureID = LoadTGA("Image//robotArm1.tga");
	meshList[GEO_MODEL2]->material.kAmbient.Set(0.6f, 0.6f, 0.6f);
	meshList[GEO_MODEL2]->material.kDiffuse.Set(0.3f, 0.3f, 0.3f);
	meshList[GEO_MODEL2]->material.kSpecular.Set(0.6f, 0.6f, 0.6f);
	meshList[GEO_MODEL2]->material.kShininess = 1.f;

	meshList[GEO_MODEL3] = MeshBuilder::GenerateOBJ("model3", "OBJ//robotArm2.obj");
	meshList[GEO_MODEL3]->textureID = LoadTGA("Image//robotArm2.tga");
	meshList[GEO_MODEL3]->material.kAmbient.Set(0.6f, 0.6f, 0.6f);
	meshList[GEO_MODEL3]->material.kDiffuse.Set(0.3f, 0.3f, 0.3f);
	meshList[GEO_MODEL3]->material.kSpecular.Set(0.6f, 0.6f, 0.6f);
	meshList[GEO_MODEL3]->material.kShininess = 1.f;

	//wheels
	meshList[GEO_BackWheel] = MeshBuilder::GenerateOBJ("wheel", "OBJ//BackWheels.obj");
	meshList[GEO_BackWheel]->textureID = LoadTGA("Image//WheelsTexture.tga");
	meshList[GEO_FrontWheel] = MeshBuilder::GenerateOBJ("wheel", "OBJ//FrontWheel.obj");
	meshList[GEO_FrontWheel]->textureID = LoadTGA("Image//WheelsTexture.tga");
	//car 1
	meshList[GEO_Carbody] = MeshBuilder::GenerateOBJ("car1", "OBJ//CarBody.obj");
	meshList[GEO_Carbody]->textureID = LoadTGA("Image//CarTextureBlue.tga");
	//car 2
	meshList[GEO_CarBody2] = MeshBuilder::GenerateOBJ("car2", "OBJ//CarBody.obj");
	meshList[GEO_CarBody2]->textureID = LoadTGA("Image//CarTextureRed.tga");
	//car 3
	meshList[GEO_CarBody3] = MeshBuilder::GenerateOBJ("car3", "OBJ//CarBody.obj");
	meshList[GEO_CarBody3]->textureID = LoadTGA("Image//CarTextureYellow.tga");
	//magnet
	meshList[GEO_MAGNET] = MeshBuilder::GenerateOBJ("magnet", "OBJ//magnet.obj");
	meshList[GEO_MAGNET]->textureID = LoadTGA("Image//magnet.tga");
	//banana
	meshList[GEO_Banana] = MeshBuilder::GenerateOBJ("banana", "OBJ//banana.obj");
	meshList[GEO_Banana]->textureID = LoadTGA("Image//banana.tga");

	//end game
	meshList[leaderBoard] = MeshBuilder::GenerateOBJ("leaderboard", "OBJ//gameOver.obj");
	meshList[leaderBoard]->textureID = LoadTGA("Image//gameOver.tga");

	//Text
	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");

	// Make sure you pass uniform parameters after glUseProgram()
	glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], light[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_COSINNER], light[0].cosInner);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], light[0].exponent);

	glUniform1i(m_parameters[U_NUMLIGHTS], 1);
	//
}

void Scene1::Update(double dt)
{
	FPS = (int)(1 / dt); // for fps

	// Check if day is close to night
	if (light[0].power <= 0.3f)
	{
		dayNightCheck = true;
	}
	// Check if day is the brightest
	if (light[0].power >= 1.5f)
	{
		dayNightCheck = false;
	}
	// increase brightness when night
	if (dayNightCheck == true)
	{
		// quantity x dt = rate of change
		light[0].power += 0.1f * dt;
		/*light[0].color.Set(
			colorValR += 0.5f * dt,
			colorValG += 0.5f * dt,
			0.862);*/
		glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
		glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	}
	// decrease brightness when day
	if (dayNightCheck == false)
	{
		light[0].power -= 0.1f * dt;
		/*light[0].color.Set(
			colorValR -= 0.5f * dt,
			colorValG -= 0.5f * dt,
			0.862);*/
		glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
		glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);

	}
	
	if (car.getTime() > 300)
	{
		static int rotateDir = 1;
		static int rotateDir1 = -1;

		if (rotateArm * rotateDir > 2)
			rotateDir = -rotateDir;
		rotateArm += (float)(rotateDir * 10 * dt);

		if (rotateArm1 * rotateDir1 > 2)
			rotateDir1 = -rotateDir1;
		rotateArm1 += (float)(rotateDir1 * 10 * dt);

		if (mapNumber == 2) {
			if (rotateArm * rotateDir > 1)
				rotateDir = -rotateDir;
			rotateArm += (float)(rotateDir * 5 * dt);

			if (rotateArm1 * rotateDir1 > 1)
				rotateDir1 = -rotateDir1;
			rotateArm1 += (float)(rotateDir1 * 5 * dt);
		}
	}

	//normals of 2 vehicles
	Vector3 normal[4];
	normal[0] = car.getCview();
	normal[1] = car.getCright();
	normal[2] = car2.getCview2();
	normal[3] = car2.getCright2();

	//car to car collision----------------------------------------
	for (int i = 0; i < 4; i++)
	{
		car.Collision.CreateCarCol(normal[i], car.getCpos(), car.getAngle());
		car2.Collision2.CreateCarCol(normal[i], car2.getCpos2(), car2.getAngle2());

		if (car.Collision.getHighest() > car2.Collision2.getHighest())
		{
			if (car.Collision.getLowest() < car2.Collision2.getHighest())
				Col[i] = true;
		}
		else if (car2.Collision2.getLowest() < car.Collision.getHighest())
			Col[i] = true;

		if (!Col[i])
		{
			car.setCollided(false);
			car2.setCollided2(false);
			break;
		}

		if (i == 3)
		{
			car.setCposition(car.getPrePos());//intersects
			car2.setCposition2(car2.getPrePos2());//intersects
			car.setCollided(true);
			car2.setCollided2(true);
		}

	}
	for (int a = 0; a < 4; a++)
	{
		Col[a] = false;
	}

	//wall collision for player 1
	Vector3 normalW[4];
	normalW[0] = car.getCview();
	normalW[1] = car.getCright();

	for (int wallNumber = 0; wallNumber < numberOfWalls; wallNumber++)
	{
		normalW[2] = (Vector3(WallBoxes[wallNumber].Pt[0]) - Vector3(WallBoxes[wallNumber].Pt[1])).Normalized();
		normalW[3] = (Vector3(WallBoxes[wallNumber].Pt[0]) - Vector3(WallBoxes[wallNumber].Pt[2])).Normalized();

		for (int i = 0; i < 4; i++)
		{
			car.Collision.CreateCarCol(normalW[i], car.getCpos(), car.getAngle());
			WallBoxes[wallNumber].CheckWalls(normalW[i]);

			if (car.Collision.getHighest() > WallBoxes[wallNumber].getHighest())
			{
				if (car.Collision.getLowest() < WallBoxes[wallNumber].getHighest())
					Col[i] = true;
			}
			else if (WallBoxes[wallNumber].getLowest() < car.Collision.getHighest())
				Col[i] = true;

			if (!Col[i])
			{
				car.setCollidedWall(false);
				break;
			}

			if (i == 3)
			{
				//collides
				car.setCposition(car.getPrePos());
				car.setCollidedWall(true);
				car.setSpeedTo0();
				car.update(dt);
				car.setCollidedWall(false);
				car.update(dt);
			}
		}
		for (int a = 0; a < 4; a++)
		{
			Col[a] = false;
		}
	}

	//wall collision for player 2
	normalW[0] = car2.getCview2();
	normalW[1] = car2.getCright2();

	for (int wallNumber = 0; wallNumber < numberOfWalls; wallNumber++)
	{
		normalW[2] = (Vector3(WallBoxes[wallNumber].Pt[0]) - Vector3(WallBoxes[wallNumber].Pt[1])).Normalized();
		normalW[3] = (Vector3(WallBoxes[wallNumber].Pt[0]) - Vector3(WallBoxes[wallNumber].Pt[2])).Normalized();
														 
		for (int i = 0; i < 4; i++)
		{
			car2.Collision2.CreateCarCol(normalW[i], car2.getCpos2(), car2.getAngle2());
			WallBoxes[wallNumber].CheckWalls(normalW[i]);

			if (car2.Collision2.getHighest() > WallBoxes[wallNumber].getHighest())
			{
				if (car2.Collision2.getLowest() < WallBoxes[wallNumber].getHighest())
					Col[i] = true;
			}
			else if (WallBoxes[wallNumber].getLowest() < car2.Collision2.getHighest())
				Col[i] = true;

			if (!Col[i])
			{
				car2.setCollidedWall2(false);
				break;
			}

			if (i == 3)
			{
				//collides
				car2.setCposition2(car2.getPrePos2());
				car2.setCollidedWall2(true);
				car2.setSpeedTo0();
				car2.update(dt);
				car2.setCollidedWall2(false);
				car2.update(dt);
			}
		}
		for (int a = 0; a < 4; a++)
		{
			Col[a] = false;
		}
	}

	//diagonal wall collision for player 1
	/*normalW[0] = car.getCview();
	normalW[1] = car.getCright();
	normalW[2] = (Vector3(-296.1, 0, -189.5) - Vector3(112.6, 0, -234.56)).Normalized();

	normalW[3] = Vector3();

	for (int wallNumber = 0; wallNumber < numberOfWalls; wallNumber++)
	{
		for (int i = 0; i < 4; i++)
		{
			car.Collision.CreateCarCol(normalW[i], car.getCpos(), car.getAngle());
			WallBoxes[wallNumber].CheckWalls(normalW[i]);

			if (car.Collision.getHighest() > WallBoxes[wallNumber].getHighest())
			{
				if (car.Collision.getLowest() < WallBoxes[wallNumber].getHighest())
					Col[i] = true;
			}
			else if (WallBoxes[wallNumber].getLowest() < car.Collision.getHighest())
				Col[i] = true;

			if (!Col[i])
			{
				car.setCollided(false);
				break;
			}

			if (i == 3)
			{
				//collides
				car.setCposition(car.getPrePos());
				car.setCollided(true);
			}
		}
		for (int a = 0; a < 4; a++)
		{
			Col[a] = false;
		}
	}

	//diagonal wall collision for player 2
	normalW[0] = car2.getCview2();
	normalW[1] = car2.getCright2();

	for (int wallNumber = 0; wallNumber < numberOfWalls; wallNumber++)
	{
		for (int i = 0; i < 4; i++)
		{
			car2.Collision2.CreateCarCol(normalW[i], car2.getCpos2(), car2.getAngle2());
			WallBoxes[wallNumber].CheckWalls(normalW[i]);

			if (car2.Collision2.getHighest() > WallBoxes[wallNumber].getHighest())
			{
				if (car2.Collision2.getLowest() < WallBoxes[wallNumber].getHighest())
					Col[i] = true;
			}
			else if (WallBoxes[wallNumber].getLowest() < car2.Collision2.getHighest())
				Col[i] = true;

			if (!Col[i])
			{
				car2.setCollided2(false);
				break;
			}

			if (i == 3)
			{
				//collides
				car2.setCposition2(car2.getPrePos2());
				car2.setCollided2(true);
			}
		}
		for (int a = 0; a < 4; a++)
		{
			Col[a] = false;
		}
	}*/
	
	//banana to car collision
	if (car.isbanana)
	{
		//get banana normals and car2 normals and loop to check if car2 and banana collided
		float d = ((bananaPos.x * bananaPos.x) + (bananaPos.y * bananaPos.y) + (bananaPos.z * bananaPos.z));
		normals[0] = Vector3((-bananaPos.x / d), (-bananaPos.y / d), (-bananaPos.z / d));
		normals[1] = normals[0].Cross(Vector3(0, 1, 0)); //side
		normals[2] = car2.getCview2();
		normals[3] = car2.getCright2();

		for (int i = 0; i < 4; i++)
		{
			car2.Collision2.CreateCarCol(normals[i], car2.getCpos2(), car2.getAngle2());
			Collide.CreateBananaCol(normals[i], bananaPos);

			if (Collide.BHighest > car2.Collision2.getHighest())
			{
				if (Collide.BLowest < car2.Collision2.getHighest())
					BananaCol[i] = true;
			}
			else if (car2.Collision2.getLowest() < Collide.BHighest)
				BananaCol[i] = true;

			if (!BananaCol[i])
				break;

			if (i == 3)//when car2 touch banana
			{
				car.setActiveBanana(0.3f);//spin car2 for awhile
				bananaPos.Set(NULL, NULL, NULL);//throw bananaPos = NULL
				car2.collideBanana2 = true;//stop car2 acceleration when true
				car.isbanana = false;//disable car1 banana render and collision function
			}
		}
	}
	if (car2.isbanana2)//when car2 spawn banana
	{
		float d = ((bananaPos2.x * bananaPos2.x) + (bananaPos2.y * bananaPos2.y) + (bananaPos2.z * bananaPos2.z));
		normals[0] = Vector3((-bananaPos2.x / d), (-bananaPos2.y / d), (-bananaPos2.z / d)); //front
		normals[1] = normals[0].Cross(Vector3(0, 1, 0)); //side
		normals[2] = car.getCview();
		normals[3] = car.getCright();
		for (int i = 0; i < 4; i++)
		{
			car.Collision.CreateCarCol(normals[i], car.getCpos(), car.getAngle());
			Collide.CreateBananaCol(normals[i], bananaPos2);

			if (Collide.BHighest > car.Collision.getHighest())
			{
				if (Collide.BLowest < car.Collision.getHighest())
					BananaCol[i] = true;
			}
			else if (car.Collision.getLowest() < Collide.BHighest)
				BananaCol[i] = true;

			if (!BananaCol[i])
				break;

			if (i == 3)//when car1 touch banana
			{
				car2.setActiveBanana(0.5f);//spin car1 for awhile
				bananaPos2.Set(NULL, NULL, NULL);//throw bananaPos2 = NULL
				car.collideBanana = true;//stop car1 acceleration when true
				car2.isbanana2 = false;//disable car2 banana render and collision function
			}
		}
	}
	for (int a = 0; a < 4; a++)//reset when not all is true
	{
		BananaCol[a] = false;
	}
	if (car.getActiveBanana() > 0.2f && car.getActiveBanana() < 5.0f)
	{
		car2.setSpeed2(50, dt);//slow down car2
		car2.setAngle2(500, dt);//rotate car2
		car.CalBanana(5, dt);//timer for this function
	}
	else
	car2.collideBanana2 = false;
	if (car2.getActiveBanana() > 0.2f && car2.getActiveBanana() < 5.0f)
	{
		car.setSpeed(50, dt);//slow down car1
		car.setAngle(500, dt);//rotate car1
		car2.CalBanana2(5, dt);// timer for this function
	}
	else 
		car.collideBanana = false;

	//lap to car collision
	if (car1lap < 3)
	{
		Vector3 lapNorm[4];
		lapNorm[0] = car.getCview();
		lapNorm[1] = car.getCright();
		lapNorm[2] = Collide.lapsNormal[0];
		lapNorm[3] = Collide.lapsNormal[1];
		//lap to car collision
		for (int i = 0; i < 4; i++)
		{
			car.Collision.CreateCarCol(lapNorm[i], car.getCpos(), car.getAngle());
			Collide.CreateLapCol(lapNorm[i]);

			//checking car1 to lab
			if (Collide.LapHighest > car.Collision.getHighest())
			{
				if (Collide.LapLowest < car.Collision.getHighest())
					carToLaps[i] = true;
			}
			else if (car.Collision.getLowest() < Collide.LapHighest)
				carToLaps[i] = true;
			if (!carToLaps[i])
				break;
			if (i == 3)//when car touch laps
				isTouched = true;

		}
		//reset when not all is true
		for (int a = 0; a < 4; a++)
		{
			carToLaps[a] = false;
		}

		//check if car collided, increase lap. if lap = 3 record time
		if (isTouched && (delaylapTime > 14.9f))//car 1
		{
			car1lap++;
			if (car1lap >= 3)
			{
				lapTime = car.getTime(); //to get time for final lap
				lapTime -= 300;
			}
			delaylapTime = 0.0f;
		}
	}
	//lap to car2 collision
	if (car2lap < 3)
	{
		Vector3 lapNorms[4];
		lapNorms[0] = car2.getCview2();
		lapNorms[1] = car2.getCright2();
		lapNorms[2] = Collide.lapsNormal[0];
		lapNorms[3] = Collide.lapsNormal[1];
		//lap to car2 collision
		for (int i = 0; i < 4; i++)
		{
			car2.Collision2.CreateCarCol(lapNorms[i], car2.getCpos2(), car2.getAngle2());
			Collide.CreateLapCol(lapNorms[i]);

			//checking car2 to lap
			if (Collide.LapHighest > car2.Collision2.getHighest())
			{
				if (Collide.LapLowest < car2.Collision2.getHighest())
					car2ToLaps[i] = true;
			}
			else if (car2.Collision2.getLowest() < Collide.LapHighest)
				car2ToLaps[i] = true;
			if (!car2ToLaps[i])
				break;
			if (i == 3)//when car2 touch laps
				isTouched2 = true;

		}
		//reset when not all is true
		for (int a = 0; a < 4; a++)
		{
			car2ToLaps[a] = false;
		}

		if (isTouched2 && (delaylapTime2 > 14.9f))//car 2
		{
			car2lap++;
			if (car2lap >= 3)
			{
				lapTime2 = car2.getTime2(); //to get time for final lap
				lapTime2 -= 300;
			}
			delaylapTime2 = 0.0f;
		}
	}
	//reset delay timer for laps
	if (delaylapTime <= 15.0f)
	{
		delaylapTime += (float)(5 * dt);
		isTouched = false;
	}
	if (delaylapTime2 <= 15.0f)
	{
		delaylapTime2 += (float)(5 * dt);
		isTouched2 = false;
	}
	//when lap = 3,
	if (car1lap >= 3)
	{
		car.collideBanana = true;
		isTouched = true;
	}
	else if (car2lap >= 3)
	{
		car2.collideBanana2 = true;
		isTouched2 = true;
	}

	car.update(dt);
	car2.update(dt);
	camera.Update(car.getCview(), car.getCpos());
	player2Camera.Update(car2.getCview2(), car2.getCpos2());
	backCamera.Update(car.getCview(), car.getCpos());
	backCamera2.Update(car2.getCview2(), car2.getCpos2());
	miniMapCamera.Update(dt);
}

void Scene1::RenderMesh(Mesh *mesh, bool enableLight)
{
	Mtx44 MVP, modelView, modelView_inverse_transpose;
	MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);
	if (enableLight)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);
		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE,
			&modelView_inverse_transpose.a[0]);
		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	}

	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}
	mesh->Render(); //this line should only be called once
	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void Scene1::RenderKart()
{
	if (P1CarNumber == 1) //blue car, magnet
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[GEO_Carbody], true);
		modelStack.PopMatrix();
		car2.carChosen = 1;
		car.maxspeed1 = 80; //stats for car1
		car.nitrospd1 = 250;
		car.nitrodur1 = 40;
		car.nitrocd1 = 200;
		car2.powerdur1 = 200;
		car2.powercd1 = 400;
	}
	else if (P1CarNumber == 2) //red car, hack
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[GEO_CarBody2], true);
		modelStack.PopMatrix();
		car2.carChosen = 2;
		car.maxspeed1 = 100; //stats for car2
		car.nitrospd1 = 200;
		car.nitrodur1 = 30;
		car.nitrocd1 = 250;
		car2.powerdur1 = 250;
		car2.powercd1 = 300;
	}
	else if (P1CarNumber == 3) //yellow car, banana
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[GEO_CarBody3], true);
		modelStack.PopMatrix();
		car2.carChosen = 3;
		car.maxspeed1 = 120; //stats for car3
		car.nitrospd1 = 150;
		car.nitrodur1 = 20;
		car.nitrocd1 = 300;
		car2.powerdur1 = 0;
		car2.powercd1 = 200;
	}
}

void Scene1::RenderKart2()
{
	if (P2CarNumber == 1) //blue car, magnet
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[GEO_Carbody], true);
		modelStack.PopMatrix();
		car.carChosen = 1;
		car2.maxspeed2 = 80; //stats for car1
		car2.nitrospd2 = 250;
		car2.nitrodur2 = 40;
		car2.nitrocd2 = 200;
		car.powerdur2 = 200;
		car.powercd2 = 400;
	}
	if (P2CarNumber == 2) //red car, hack
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[GEO_CarBody2], true);
		modelStack.PopMatrix();
		car.carChosen = 2;
		car2.maxspeed2 = 100; //stats for car2
		car2.nitrospd2 = 200;
		car2.nitrodur2 = 30;
		car2.nitrocd2 = 250;
		car.powerdur2 = 250;
		car.powercd2 = 300;
	}
	if (P2CarNumber == 3) //yellow car, banana
	{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[GEO_CarBody3], true);
		modelStack.PopMatrix();
		car.carChosen = 3;
		car2.maxspeed2 = 120; //stats for car3
		car2.nitrospd2 = 150;
		car2.nitrodur2 = 20;
		car2.nitrocd2 = 300;
		car.powerdur2 = 0;
		car.powercd2 = 200;
	}
}

void Scene1::RenderFrontWheels()
{
	modelStack.PushMatrix();
	modelStack.Translate(2.2f, -1.2f, 4.4f);
	modelStack.Rotate(0, 1, 0, 0);
	modelStack.Scale(2, 2, 2);
	RenderMesh(meshList[GEO_FrontWheel], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-2.2f, -1.2f, 4.4f);
	modelStack.Rotate(0, 1, 0, 0);
	modelStack.Scale(2, 2, 2);
	RenderMesh(meshList[GEO_FrontWheel], true);
	modelStack.PopMatrix();
}

void Scene1::RenderBackWheels()
{
	modelStack.PushMatrix();
	modelStack.Translate(0, -1.2f, -4.5f);
	modelStack.Rotate(0, 1, 0, 0);
	modelStack.Scale(2, 2, 2);
	RenderMesh(meshList[GEO_BackWheel], true);
	modelStack.PopMatrix();
}

void Scene1::RenderRobot() //f49c2a
{
	if (car.getTime() < 1000)
	{
		if (mapNumber == 1)
		{
			modelStack.PushMatrix();
			modelStack.Translate(2, 2, 0);
			modelStack.Rotate(180, 0, -1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL1], true);
			modelStack.PopMatrix();
		}
		else if (mapNumber == 2)
		{
			modelStack.PushMatrix();
			modelStack.Translate(85, 2, 30);
			modelStack.Rotate(180, 0, 1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL1], true);
			modelStack.PopMatrix();
		}
		else
		{
			modelStack.PushMatrix();
			modelStack.Translate(5, 2, -5);
			modelStack.Rotate(180, 0, 1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL1], true);
			modelStack.PopMatrix();
		}
	}
}


void Scene1::RenderRobotA1()
{
	if (car.getTime() < 1000)
	{
		if (mapNumber == 1)
		{
			modelStack.PushMatrix();
			modelStack.Translate(2, 2, 0);
			modelStack.Rotate(180, 0, -1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL2], true);
			modelStack.PopMatrix();
		}
		else if (mapNumber == 2)
		{
			modelStack.PushMatrix();
			modelStack.Translate(85, 2, 30);
			//modelStack.Rotate(180, 0, 1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL2], true);
			modelStack.PopMatrix();
		}
		else
		{
			modelStack.PushMatrix();
			modelStack.Translate(5, 2, -5);
			modelStack.Rotate(180, 0, 1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL2], true);
			modelStack.PopMatrix();
		}
	}
}

void Scene1::RenderRobotA2()
{
	if (car.getTime() < 1000)
	{
		if (mapNumber == 1)
		{
			modelStack.PushMatrix();
			modelStack.Translate(2, 2, 0);
			modelStack.Rotate(180, 0, -1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL3], true);
			modelStack.PopMatrix();
		}
		else if (mapNumber == 2)
		{
			modelStack.PushMatrix();
			modelStack.Translate(85, 2, 30);
			//modelStack.Rotate(180, 0, 1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL3], true);
			modelStack.PopMatrix();
		}
		else
		{
			modelStack.PushMatrix();
			modelStack.Translate(5, 2, -5);
			modelStack.Rotate(180, 0, 1, 0);
			modelStack.Scale(3, 3, 3);
			RenderMesh(meshList[GEO_MODEL3], true);
			modelStack.PopMatrix();
		}
	}
}

void Scene1::RenderBanana()
{
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(1, 1, 1);
		RenderMesh(meshList[GEO_Banana], false);
		modelStack.PopMatrix();
}

void Scene1::RenderEndGame()
{
	modelStack.PushMatrix();
	modelStack.Translate(98.2f, 97.25f, 98.2f);
	modelStack.Rotate(0, 0, 1, 0);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[leaderBoard], false);
	modelStack.PopMatrix();
}

void Scene1::box()
{
	modelStack.PushMatrix();
	modelStack.Translate(0, 0, 0);
	modelStack.Rotate(0, 1, 0, 0);
	modelStack.Scale(1.5f, 1.5f, 1.5f);
	RenderMesh(meshList[GEO_CUBE1], true);
	modelStack.PopMatrix();
}

void Scene1::magnet()
{
	modelStack.PushMatrix();
	modelStack.Translate(0, 0, 0);
	modelStack.Scale(1.f, 1.f, 1.f);
	RenderMesh(meshList[GEO_MAGNET], false);
	modelStack.PopMatrix();
}

static const float SKYBOXSIZE = 1000.f;
void Scene1::RenderSkybox() //render skybox
{
	modelStack.PushMatrix();
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Translate(0, 0, -SKYBOXSIZE / 2 + 2.f);
	modelStack.Scale(SKYBOXSIZE, SKYBOXSIZE, SKYBOXSIZE);
	RenderMesh(meshList[GEO_LEFT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(-90, 0, 1, 0);
	modelStack.Translate(0, 0, -SKYBOXSIZE / 2 + 2.f);
	modelStack.Scale(SKYBOXSIZE, SKYBOXSIZE, SKYBOXSIZE);
	RenderMesh(meshList[GEO_RIGHT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -SKYBOXSIZE / 2 + 2.f);
	modelStack.Scale(SKYBOXSIZE, SKYBOXSIZE, SKYBOXSIZE);
	RenderMesh(meshList[GEO_FRONT], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(180, 0, 1, 0);
	modelStack.Translate(0, 0, -SKYBOXSIZE / 2 + 2.f);
	modelStack.Scale(SKYBOXSIZE, SKYBOXSIZE, SKYBOXSIZE);
	RenderMesh(meshList[GEO_BACK], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(90, 1, 0, 0);
	modelStack.Translate(0, 0, -SKYBOXSIZE / 2 + 2.f);
	modelStack.Rotate(90, 0, 0, 1);
	modelStack.Scale(SKYBOXSIZE, SKYBOXSIZE, SKYBOXSIZE);
	RenderMesh(meshList[GEO_TOP], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Translate(0, 0, -SKYBOXSIZE / 2 + 2.f);
	modelStack.Rotate(-90, 0, 0, 1);
	modelStack.Scale(SKYBOXSIZE, SKYBOXSIZE, SKYBOXSIZE);
	RenderMesh(meshList[GEO_BOTTOM], false);
	modelStack.PopMatrix();
}

void Scene1::RenderText(Mesh* mesh, std::string text, Color color)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;
	glDisable(GL_DEPTH_TEST);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
			Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() *
			characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	glEnable(GL_DEPTH_TEST);
}

void Scene1::RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;
	glDisable(GL_DEPTH_TEST);

	//Add these code just after glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 0.5f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() *
			characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	//Add these code just before glEnable(GL_DEPTH_TEST);
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void Scene1::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Mtx44 translate, rotate, model, scale, projection, view; // MVP
	
	if (light[0].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[0].position.x, light[0].position.y, light[0].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1,
			&lightDirection_cameraspace.x);
	}
	else if (light[0].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1,
			&lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() *
			light[0].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1,
			&spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1,
			&lightPosition_cameraspace.x);
	}

	if (cameraType == 1)
	{
		projection.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 1000.0f); //FOV, Aspect Ratio, Near plane, Far plane

		viewStack.LoadIdentity();
		viewStack.LookAt(camera.getPositionX(), camera.getPositionY(), camera.getPositionZ(),
			camera.getTargetX(), camera.getTargetY(), camera.getTargetZ(), camera.getUpX(), camera.getUpY(),
			camera.getUpZ());
		modelStack.LoadIdentity();
	}
	else if (cameraType == 2)
	{
		projection.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 1000.0f); //FOV, Aspect Ratio, Near plane, Far plane

		viewStack.LoadIdentity();
		viewStack.LookAt(player2Camera.getPositionX(), player2Camera.getPositionY(), player2Camera.getPositionZ(),
			player2Camera.getTargetX(), player2Camera.getTargetY(), player2Camera.getTargetZ(), player2Camera.getUpX(), player2Camera.getUpY(),
			player2Camera.getUpZ());
		modelStack.LoadIdentity();
	}
	else if (cameraType == 3)
	{
		projection.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 1000.0f); //FOV, Aspect Ratio, Near plane, Far plane

		viewStack.LoadIdentity();
		viewStack.LookAt(backCamera.getPositionX(), backCamera.getPositionY(), backCamera.getPositionZ(),
			backCamera.getTargetX(), backCamera.getTargetY(), backCamera.getTargetZ(), backCamera.getUpX(), backCamera.getUpY(),
			backCamera.getUpZ());
		modelStack.LoadIdentity();
	}
	else if (cameraType == 4)
	{
		projection.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 1000.0f); //FOV, Aspect Ratio, Near plane, Far plane

		viewStack.LoadIdentity();
		viewStack.LookAt(backCamera2.getPositionX(), backCamera2.getPositionY(), backCamera2.getPositionZ(),
			backCamera2.getTargetX(), backCamera2.getTargetY(), backCamera2.getTargetZ(), backCamera2.getUpX(), backCamera2.getUpY(),
			backCamera2.getUpZ());
		modelStack.LoadIdentity();
	}
	else if (cameraType == 5)
	{
		projection.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 1000.0f); //FOV, Aspect Ratio, Near plane, Far plane

		viewStack.LoadIdentity();
		viewStack.LookAt(miniMapCamera.getPositionX(), miniMapCamera.getPositionY(), miniMapCamera.getPositionZ(),
			miniMapCamera.getTargetX(), miniMapCamera.getTargetY(), miniMapCamera.getTargetZ(), miniMapCamera.getUpX(), miniMapCamera.getUpY(),
			miniMapCamera.getUpZ());
		modelStack.LoadIdentity();
	}
	else if (cameraType == 6)
	{
		projection.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 1000.0f); //FOV, Aspect Ratio, Near plane, Far plane

		viewStack.LoadIdentity();
		viewStack.LookAt(endGame.getPositionX(), endGame.getPositionY(), endGame.getPositionZ(),
			endGame.getTargetX(), endGame.getTargetY(), endGame.getTargetZ(), endGame.getUpX(), endGame.getUpY(),
			endGame.getUpZ());
		modelStack.LoadIdentity();
	}

	if (cameraType == 1)
	{
		Mtx44 mvp = projectionStack.Top() * viewStack.Top() * modelStack.Top();

		view.SetToLookAt(
			camera.getPositionX(), camera.getPositionY(), camera.getPositionZ(),
			camera.getTargetX(), camera.getTargetY(), camera.getTargetZ(), camera.getUpX(), camera.getUpY(),
			camera.getUpZ()
		);
	}
	else if (cameraType == 2)
	{
		Mtx44 mvp = projectionStack.Top() * viewStack.Top() * modelStack.Top();

		view.SetToLookAt(
			player2Camera.getPositionX(), player2Camera.getPositionY(), player2Camera.getPositionZ(),
			player2Camera.getTargetX(), player2Camera.getTargetY(), player2Camera.getTargetZ(), player2Camera.getUpX(), player2Camera.getUpY(),
			player2Camera.getUpZ()
		);
	}
	else if (cameraType == 3)
	{
		Mtx44 mvp = projectionStack.Top() * viewStack.Top() * modelStack.Top();

		view.SetToLookAt(
			backCamera.getPositionX(), backCamera.getPositionY(), backCamera.getPositionZ(),
			backCamera.getTargetX(), backCamera.getTargetY(), backCamera.getTargetZ(), backCamera.getUpX(), backCamera.getUpY(),
			backCamera.getUpZ()
		);
	}
	else if (cameraType == 4)
	{
		Mtx44 mvp = projectionStack.Top() * viewStack.Top() * modelStack.Top();

		view.SetToLookAt(
			backCamera2.getPositionX(), backCamera2.getPositionY(), backCamera2.getPositionZ(),
			backCamera2.getTargetX(), backCamera2.getTargetY(), backCamera2.getTargetZ(), backCamera2.getUpX(), backCamera2.getUpY(),
			backCamera2.getUpZ()
		);
	}
	else if (cameraType == 5)
	{
		Mtx44 mvp = projectionStack.Top() * viewStack.Top() * modelStack.Top();

		view.SetToLookAt(
			miniMapCamera.getPositionX(), miniMapCamera.getPositionY(), miniMapCamera.getPositionZ(),
			miniMapCamera.getTargetX(), miniMapCamera.getTargetY(), miniMapCamera.getTargetZ(), miniMapCamera.getUpX(), miniMapCamera.getUpY(),
			miniMapCamera.getUpZ()
		);
	}
	else if (cameraType == 6)
	{
		projection.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 1000.0f); //FOV, Aspect Ratio, Near plane, Far plane

		viewStack.LoadIdentity();
		viewStack.LookAt(endGame.getPositionX(), endGame.getPositionY(), endGame.getPositionZ(),
			endGame.getTargetX(), endGame.getTargetY(), endGame.getTargetZ(), endGame.getUpX(), endGame.getUpY(),
			endGame.getUpZ());
		modelStack.LoadIdentity();
	}

	model.SetToIdentity();

	// Render Axes
	//RenderMesh(meshList[GEO_AXES], false);
	//
	RenderSkybox();
	RenderEndGame();
	RenderRobot();

	// Car movement
	modelStack.PushMatrix();
	modelStack.Translate(car.getCposx(), 0, car.getCposz());
	modelStack.Rotate(car.getAngle(), 0, 1, 0);
		modelStack.PushMatrix();
		modelStack.Translate(0, -1.2f, 4.4f);
		modelStack.Rotate(car.getrotateWheels(), 1, 0, 0);
		modelStack.Translate(0, 1.2f, -4.4f);
		RenderFrontWheels();
		modelStack.PopMatrix();
		modelStack.PushMatrix();
		modelStack.Translate(0, -1.2f, -4.5f);
		modelStack.Rotate(car.getrotateWheels(), 1, 0, 0);
		modelStack.Translate(0, 1.2f, 4.5f);
		RenderBackWheels();
		modelStack.PopMatrix();	
	RenderKart();
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(car2.getCposx2(), 0, car2.getCposz2());
	modelStack.Rotate(car2.getAngle2(), 0, 1, 0);
		modelStack.PushMatrix();
		modelStack.Translate(0, -1.2f, 4.4f);
		modelStack.Rotate(car2.getrotateWheels2(), 1, 0, 0);
		modelStack.Translate(0, 1.2f, -4.4f);
		RenderFrontWheels();
		modelStack.PopMatrix();
		modelStack.PushMatrix();
		modelStack.Translate(0, -1.2f, -4.5f);
		modelStack.Rotate(car2.getrotateWheels2(), 1, 0, 0);
		modelStack.Translate(0, 1.2f, 4.5f);
		RenderBackWheels();
		modelStack.PopMatrix();
	RenderKart2();
	modelStack.PopMatrix();

	// render magnet
	if (car.magnet2 == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(car.getCposx(), car.getCposy() + 3, car.getCposz());
		modelStack.Rotate(car.getAngle(), 0, 1, 0);
		magnet();
		modelStack.PopMatrix();
	}
	if (car2.magnet == true)
	{
		modelStack.PushMatrix();
		modelStack.Translate(car2.getCposx2(), car2.getCposy2() + 3, car2.getCposz2());
		modelStack.Rotate(car2.getAngle2(), 0, 1, 0);
		magnet();
		modelStack.PopMatrix();
	}

	//banana
	if (car.Car2putBanana)//car2 weapon
	{
		bananaPos2 = car2.getCpos2(); //takes in car2 position
		car2.isbanana2 = true;//when true, create & runs collision. if target hit collision,  set it to false
		car.Car2putBanana = false;//get current car position and set it to banana position
	}
	else
		car.Car2putBanana = false;
	if (car2.Car1putBanana)//car1 weapon
	{
		bananaPos = car.getCpos(); //takes in car1 position
		car.isbanana = true;//when true, create & runs collision. if target hit collision,  set it to false
		car2.Car1putBanana = false;//get current car position and set it to banana position
	}
	else
	{
		car2.Car1putBanana = false;
	}

	//render banana when skill is used
	if (car.isbanana)//banana skill used BY car1
	{
		modelStack.PushMatrix();
		modelStack.Translate(bananaPos.x, 0,bananaPos.z);
		RenderBanana();
		modelStack.PopMatrix();
	}
	if(car2.isbanana2)//banana skill used BY car2
	{
		modelStack.PushMatrix();
		modelStack.Translate(bananaPos2.x, 0, bananaPos2.z);
		RenderBanana();
		modelStack.PopMatrix();
	}

	if (mapNumber == 1) //map 1
	{
		// Robot Arm1
		modelStack.PushMatrix();
		modelStack.Rotate(rotateArm, 0, 0, 1);
		RenderRobotA1();
		modelStack.PopMatrix();
		// Robot Arm2
		modelStack.PushMatrix();
		modelStack.Rotate(rotateArm1, 0, 0, 1);
		RenderRobotA2();
		modelStack.PopMatrix();
	}
	else //map 2 & 3
	{
		// Robot Arm1
		modelStack.PushMatrix();
		modelStack.Rotate(rotateArm, 0, 0, 1);
		RenderRobotA1();
		modelStack.PopMatrix();
		// Robot Arm2
		modelStack.PushMatrix();
		modelStack.Rotate(rotateArm1, 0, 0, 1);
		RenderRobotA2();
		modelStack.PopMatrix();
	}

	if (mapNumber == 1)
	{
		//track 1
		//modelStack.PushMatrix();
		//modelStack.Translate(0, -2, 0);
		//modelStack.Rotate(0, 0, 1, 0);
		//modelStack.Scale(2, 2, 2);
		//RenderMesh(meshList[RACETRACK_FinishLine], true);
		//modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(0, -2, 0);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[RACETRACK_WallInner], false);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(0, -2, 0);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[RACETRACK_WallOuter], false);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(0, -2, 0);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[RACETRACK_Road], false);
		modelStack.PopMatrix();
	}
	else if (mapNumber == 2)
	{
		//track 2
		modelStack.PushMatrix();
		modelStack.Translate(0, 45, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[RACETRACK_FinishLine], true);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(0, 45, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[RACETRACK_Building], true);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(0, 45, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[RACETRACK_Road], true);
		modelStack.PopMatrix();
	}
	else
	{
		//track 3
		modelStack.PushMatrix();
		modelStack.Translate(0, -2, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[RACETRACK_WallOuter], true);
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		modelStack.Translate(0, -2, 0);
		modelStack.Rotate(0, 0, 1, 0);
		modelStack.Scale(2, 2, 2);
		RenderMesh(meshList[RACETRACK_Road], true);
		modelStack.PopMatrix();
	}

	// Must be last!
	// countdown
	if (car.getTime() > 100 && car.getTime() < 199)  // 3.
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "READY", Color(1, 0, 1), 7, 5, 4);
	}
	if (car.getTime() > 200 && car.getTime() < 299)  // 2.
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "SET", Color(1, 1, 0), 7, 5.5, 4);
	}
	if (car.getTime() > 300 && car.getTime() < 399)  // 1.
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "GO!", Color(0, 1, 1), 7, 5.5, 4);
	}

	//when both car reach lap 3. else keep render FPS
	if (car1lap >= 3 && car2lap >= 3)
	{
		std::string temp = std::to_string(lapTime) + "s";
		std::string temp2 = std::to_string(lapTime2) + "s";
		if (lapTime < lapTime2)//timing of player1 faster than player2
		{
			//car1
			if (P1CarNumber == 1)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "1st. Player 1", Color(0.1f, 0.1f, 1.f), 3.f, 6.f, 13.f);//blue
				RenderTextOnScreen(meshList[GEO_TEXT], temp, Color(0.1f, 0.1f, 1.f), 3.f, 20.f, 13.f);
			}
			else if (P1CarNumber == 2)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "1st. Player 1", Color(1.f, 0.1f, 0.1f), 3.f, 6.f, 13.f);//red
				RenderTextOnScreen(meshList[GEO_TEXT], temp, Color(1.f, 0.1f, 0.1f), 3.f, 20.f, 13.f);
			}
			else
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "1st. Player 1", Color(0.9f, 0.9f, 0.f), 3.f, 6.f, 13.f);//yellow
				RenderTextOnScreen(meshList[GEO_TEXT], temp, Color(0.9f, 0.9f, 0.f), 3.f, 20.f, 13.f);
			}
			//car2
			if (P2CarNumber == 1)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "2nd. Player 2", Color(0.1f, 0.1f, 1.f), 3.f, 6.f, 10.f);//blue
				RenderTextOnScreen(meshList[GEO_TEXT], temp2, Color(0.1f, 0.1f, 1.f), 3.f, 20.f, 10.f);
			}
			else if (P2CarNumber == 2)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "2nd. Player 2", Color(1.f, 0.1f, 0.1f), 3.f, 6.f, 10.f);//red
				RenderTextOnScreen(meshList[GEO_TEXT], temp2, Color(1.f, 0.1f, 0.1f), 3.f, 20.f, 10.f);
			}
			else
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "2nd. Player 2", Color(0.9f, 0.9f, 0.f), 3.f, 6.f, 10.f);//yellow
				RenderTextOnScreen(meshList[GEO_TEXT], temp2, Color(0.9f, 0.9f, 0.f), 3.f, 20.f, 10.f);
			}
		}
		else
		{
			//car2
			if (P2CarNumber == 1)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "1st. Player 2", Color(0.1f, 0.1f, 1.f), 3.f, 0.f, 0.f);//blue
				RenderTextOnScreen(meshList[GEO_TEXT], temp2, Color(0.1f, 0.1f, 1.f), 3.f, 20.f, 13.f);
			}
			else if (P2CarNumber == 2)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "1st. Player 2", Color(1.f, 0.1f, 0.1f), 3.f, 6.f, 13.f);//red
				RenderTextOnScreen(meshList[GEO_TEXT], temp2, Color(1.f, 0.1f, 0.1f), 3.f, 20.f, 13.f);
			}
			else
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "1st. Player 2", Color(0.9f, 0.9f, 0.f), 3.f, 6.f, 13.f);//yellow
				RenderTextOnScreen(meshList[GEO_TEXT], temp2, Color(0.9f, 0.9f, 0.f), 3.f, 20.f, 13.f);
			}
			//car1
			if (P1CarNumber == 1)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "2nd. Player 1", Color(0.1f, 0.1f, 1.f), 3.f, 6.f, 10.f);//blue
				RenderTextOnScreen(meshList[GEO_TEXT], temp, Color(0.1f, 0.1f, 1.f), 3.f, 20.f, 10.f);
			}
			else if (P1CarNumber == 2)
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "2nd. Player 1", Color(1.f, 0.1f, 0.1f), 3.f, 6.f, 10.f);//red
				RenderTextOnScreen(meshList[GEO_TEXT], temp, Color(1.f, 0.1f, 0.1f), 3.f, 20.f, 10.f);
			}
			else
			{
				RenderTextOnScreen(meshList[GEO_TEXT], "2nd. Player 1", Color(0.9f, 0.9f, 0.f), 3.f, 6.f, 10.f);//yellow
				RenderTextOnScreen(meshList[GEO_TEXT], temp, Color(0.9f, 0.9f, 0.f), 3.f, 20.f, 10.f);
			}
		}
		RenderTextOnScreen(meshList[GEO_TEXT], "Press ESC to exit...", Color(0.f, 0.f, 0.f), 3.f, 8.f, 2.f);
	}
	else
	{

		if (car1lap >= 3)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "Finish!!", Color(1, 1, 0), 3.f, 12.5f, 16.f);
		}
		if (car2lap >= 3)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "Finish!!", Color(1, 1, 0), 3.f, 12.5f, 5.5f);
		}

		RenderTextOnScreen(meshList[GEO_TEXT], "NITRO: ", Color(1, 1, 0), 3, 1, 2);
		if (car2.getCooldown2() < car2.nitrocd2)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "OFF", Color(1, 1, 0), 3, 5, 2);
		}
		if (car2.getCooldown2() == car2.nitrocd2)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "ON", Color(1, 1, 0), 3, 5, 2);
		}
		//
		RenderTextOnScreen(meshList[GEO_TEXT], "POWER: ", Color(1, 1, 0), 3, 1, 13);
		if (car2.getPcooldown() < car2.getpowercd1())
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "OFF", Color(1, 1, 0), 3, 5, 13);
		}
		if (car2.getPcooldown() == car2.getpowercd1())
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "ON", Color(1, 1, 0), 3, 5, 13);
		}

		RenderTextOnScreen(meshList[GEO_TEXT], "POWER: ", Color(1, 1, 0), 3, 1, 3);
		if (car.getPcooldown2() < car.getpowercd2())
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "OFF", Color(1, 1, 0), 3, 5, 3);
		}
		if (car.getPcooldown2() >= car.getpowercd2())
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "ON", Color(1, 1, 0), 3, 5, 3);
		}

		RenderTextOnScreen(meshList[GEO_TEXT], "FPS: ", Color(1, 1, 0), 3, 23, 1);
		RenderTextOnScreen(meshList[GEO_TEXT], std::to_string(FPS), Color(1, 1, 0), 3, 25, 1);

		RenderTextOnScreen(meshList[GEO_TEXT], "LAP: ", Color(1, 1, 0), 3, 23, 19);
		RenderTextOnScreen(meshList[GEO_TEXT], std::to_string(car1lap), Color(1, 1, 0), 3, 25, 19);
		RenderTextOnScreen(meshList[GEO_TEXT], "LAP: ", Color(1, 1, 0), 3, 23, 9);
		RenderTextOnScreen(meshList[GEO_TEXT], std::to_string(car2lap), Color(1, 1, 0), 3, 25, 9);

		RenderTextOnScreen(meshList[GEO_TEXT], "PLAYER 1", Color(0, 1, 1), 3, 1, 19);
		RenderTextOnScreen(meshList[GEO_TEXT], "PLAYER 2", Color(0, 1, 1), 3, 1, 9);

		RenderTextOnScreen(meshList[GEO_TEXT], "SPEED: ", Color(1, 1, 0), 3, 1, 11);
		RenderTextOnScreen(meshList[GEO_TEXT], std::to_string((int)car.getSpeed()), Color(1, 1, 0), 3, 5, 11);

		RenderTextOnScreen(meshList[GEO_TEXT], "SPEED: ", Color(1, 1, 0), 3, 1, 1);
		RenderTextOnScreen(meshList[GEO_TEXT], std::to_string((int)car2.getSpeed2()), Color(1, 1, 0), 3, 5, 1);

		RenderTextOnScreen(meshList[GEO_TEXT], "NITRO: ", Color(1, 1, 0), 3, 1, 12);
		if (car.getCooldown() < car.nitrocd1)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "OFF", Color(1, 1, 0), 3, 5, 12);
		}
		if (car.getCooldown() == car.nitrocd1)
		{
			RenderTextOnScreen(meshList[GEO_TEXT], "ON", Color(1, 1, 0), 3, 5, 12);
		}
	}



	if (car.getHacked())
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "FAS32;R30C289c'-o-39p'kjowjf /s =20-184j aafhJFNSKJAD12", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "jfioNIO39RUF23-4R] /-V.RP2O3J\21`=~9)+EWJIJFWmiVJ?><@-#ju3)", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "hKJN(#0-j#+@4O3VHJUI3-1] \32IHF3O09j3nkvjr)#JRNF#)_3[+~~~J92", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "EBiuF*939y8h*y#)!r*)31871!&`-_vjcgR93hyuug1igri9oI#RH0931ih10-", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "NHBef8912b*#($)!~0(347()adh*#$@()%U}|\'JAjfAO*KFENQ(@!&$njfui", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "HUFEU*#N#%I()@N~()#HN#(~!)HTIU$_+CmC>[}<CWQi@)(rhn@)($!@)RNV+#", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "un#893UNg3U1*NZGDm3-1m*138N1)1-MMNHDU]12}{1mP<193#m*1YN1MmdbYG", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "ndu3983N83H101+)jN~=2IjIEB@#()jheNB19JebHENENCVXL//-9H22hNDJ298b", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "!hvdbiu178012-0EI=!U80!+18HDBNJ12*)9-1+i)!(2Y8hb!2UGbHBH1989I=H2j", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
		RenderTextOnScreen(meshList[GEO_TEXT], "N&6&#@(8@#02nHudh@MHsHU2m9(!`=m,?:}}{K92M@92m*@M2*M,@N3N013N*)", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 20 + 11);
	}
	if (car2.getHacked())
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "N&6&#@(8@#02nHudh@MHsHU2m9(!`=m,?:}}{K92M@92m*@M2*M,@N3N013N*)", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 10 + 0);
		RenderTextOnScreen(meshList[GEO_TEXT], "HDAH8#(#)93u-19u_H#(2-1JP$P:#{|V}<>?>Cp189-(IV#JLk1;39P_VIp1i3", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 10 + 0);
		RenderTextOnScreen(meshList[GEO_TEXT], "*Y)(URPQH@IO#__~{<LmjLHIOHR()U@JRNKL#!I)U)~P(UU)_@I$){I~:MJERO@", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 10 + 0);
		RenderTextOnScreen(meshList[GEO_TEXT], "ih*Y#OiHG(#_OUG~}KCklLHO*~)`h`bo9o	lk./~?LJO *U)~J jbo`y po:H*0", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 10 + 0);
		RenderTextOnScreen(meshList[GEO_TEXT], "HU&*&^@OH IU(!CI+_]/-lQBU*@hciklhO*@4HINclkHIucyOL>C?C:#{C|C2u09j", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 10 + 0);
		RenderTextOnScreen(meshList[GEO_TEXT], "E&Y@)@(_~JI:RJP#PIRBLJ#RCN@IUHOV~|C>/'MJC@(@HC(~&hJBKJ<C PU(@U2(", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 10 + 0);
		RenderTextOnScreen(meshList[GEO_TEXT], "KNU&^02[:VJ{@Nc8yoL@Cn4hv2/u;$|$(u94ucbBcnu2Y98YIUH#CJG&*ROI#rh38", Color(0.f, 1.f, 0.f), 3.f, 0.f, rand() % 10 + 0);
		RenderTextOnScreen(meshList[GEO_TEXT], "KFYVT19b0-()!_C}M}C{ YSB@(UCPU@~(YCO@UEECYB@}((OK?$$:@$U~~O()CYH@KL", Color(0.f, 1.f, 0.f), 3.f, 0.f, (float)(rand() % 10 + 0));
	}
}

void Scene1::Exit()
{
	// Cleanup VBO here
	glDeleteProgram(m_programID);
}

void Scene1::setCameraType(int a)
{
	cameraType = a;
}

int Scene1::getMapNumber()
{
	return this->mapNumber;
}

void Scene1::setMapNumber(int number)
{
	mapNumber = number;
}

int Scene1::getP1Car()
{
	return this->P1CarNumber;
}

void Scene1::setP1Car(int car1)
{
	P1CarNumber = car1;
}

int Scene1::getP2Car()
{
	return this->P2CarNumber;
}

void Scene1::setP2Car(int car2)
{
	P2CarNumber = car2;
}