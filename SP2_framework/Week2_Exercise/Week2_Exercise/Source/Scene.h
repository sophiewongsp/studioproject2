#ifndef SCENE_H
#define SCENE_H

class Scene
{
public:
	Scene() {}
	~Scene() {}

	virtual void Init() = 0;
	virtual void Update(double dt) = 0;
	virtual void Render() = 0;
	virtual void Exit() = 0;
	virtual void setCameraType(int a) = 0;
	virtual int getMapNumber() = 0;
	virtual void setMapNumber(int number) = 0;
	virtual int getP1Car() = 0;
	virtual void setP1Car(int car1) = 0;
	virtual int getP2Car() = 0;
	virtual void setP2Car(int car2) = 0;
	bool FP1 = false;
	bool FP2 = false;

	bool PlayHL = true;
	bool InsHL = false;
	int mapNumber = 1;
	int P1CarNumber = 1;
	int P2CarNumber = 1;
	int ChooseMapCount = 0;
	int car1lap, car2lap = 0;
};

#endif