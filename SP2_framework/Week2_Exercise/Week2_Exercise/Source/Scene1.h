#ifndef SCENE_1_H
#define SCENE_1_H

#include <stdlib.h>
#include <time.h>
#include "Scene.h"
#include "Camera2.h"
#include "Mesh.h"
#include "MeshBuilder.h"
#include "MatrixStack.h"
#include "Light.h"
#include "Car.h"
#include "Car2.h"
#include "CheckCollision.h"

class Scene1 : public Scene
{
	enum UNIFORM_TYPE
	{
		U_MVP = 0,
		U_MODELVIEW,
		U_MODELVIEW_INVERSE_TRANSPOSE,
		U_MATERIAL_AMBIENT,
		U_MATERIAL_DIFFUSE,
		U_MATERIAL_SPECULAR,
		U_MATERIAL_SHININESS,

		U_LIGHT0_POSITION,
		U_LIGHT0_COLOR,
		U_LIGHT0_POWER,
		U_LIGHT0_KC,
		U_LIGHT0_KL,
		U_LIGHT0_KQ,

		//add these enum in UNIFORM_TYPE before U_TOTAL
		U_LIGHT0_TYPE,
		U_LIGHT0_SPOTDIRECTION,
		U_LIGHT0_COSCUTOFF,
		U_LIGHT0_COSINNER,
		U_LIGHT0_EXPONENT,

		//add these enum in UNIFORM_TYPE before U_TOTAL
		U_NUMLIGHTS, //in case you missed out practical 7
		U_LIGHTENABLED,
		U_COLOR_TEXTURE_ENABLED,
		U_COLOR_TEXTURE,
		U_TEXT_ENABLED,
		U_TEXT_COLOR,
		U_TOTAL,
	};

	enum GEOMETRY_TYPE
	{
		GEO_CUBE,
		GEO_AXES,
		GEO_LIGHTBALL,
		//add these enum in GEOMETRY_TYPE before NUM_GEOMETRY
		GEO_LEFT,
		GEO_RIGHT,
		GEO_TOP,
		GEO_BOTTOM,
		GEO_FRONT,
		GEO_BACK,
		GEO_TRACK1,
		GEO_MODEL1,
		GEO_MODEL2,
		GEO_MODEL3,
		GEO_TEXT,
		GEO_FrontWheel,
		GEO_BackWheel,
		GEO_Carbody,
		GEO_CarBody2,
		GEO_CarBody3,
		GEO_MAGNET,
		RACETRACK_Road,
		RACETRACK_WallOuter,
		RACETRACK_WallInner,
		RACETRACK_FinishLine,
		RACETRACK_Building,
		GEO_Banana,

		leaderBoard,

		GEO_CUBE1,// check axis
		GEO_CUBE2,// check axis
		NUM_GEOMETRY,
	};
public:
	Scene1();
	~Scene1();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();
	MS modelStack, viewStack, projectionStack;
	virtual int getMapNumber();
	virtual void setMapNumber(int number);
	virtual void setCameraType(int a);
	virtual int getP1Car();
	virtual void setP1Car(int car1);	
	virtual int getP2Car();
	virtual void setP2Car(int car2);
	int cameraType = 0;
	int numberOfWalls = 0;

private:
	unsigned m_vertexArrayID;
	Mesh* meshList[NUM_GEOMETRY];
	unsigned m_programID;
	unsigned m_parameters[U_TOTAL];
	float rotateArm, rotateArm1;
	int colorValR = 0.1;
	int colorValG = 0.6;
	int colorValB = 0.1;
	bool dayNightCheck = true; //bool to check for day and night ( True = day, False = night )

	float delaylapTime, delaylapTime2, cubeX, cubeZ; //checking axis(can delete)
	int FPS, lapTime, lapTime2;
	//for lap
	bool isTouched, isTouched2;
	//bool to check collision
	bool BananaCol[4], Col[4], carToLaps[4], car2ToLaps[4];

	Vector3 bananaPos, bananaPos2, normals[4];
	Camera2 camera, backCamera, backCamera2, player2Camera, miniMapCamera, endGame;
	Car car;
	Car2 car2;
	CheckCollision Collide;
	CheckCollision WallBoxes[100];

	void RenderSkybox();
	void RenderKart();
	void RenderKart2();
	void RenderFrontWheels();
	void RenderBackWheels();
	void RenderRobot();
	void RenderRobotA1();
	void RenderRobotA2();
	void RenderBanana();
	void RenderEndGame();
	void box();	//box to check axis(can delete)
	void magnet();
	void RenderText(Mesh* mesh, std::string text, Color color);
	void RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y);
	Light light[1];
	void RenderMesh(Mesh *mesh, bool enableLight);
};

#endif