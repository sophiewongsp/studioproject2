#pragma once
#include "Scene.h"
#include "Camera2.h"
#include "Mesh.h"
#include "MeshBuilder.h"
#include "MatrixStack.h"
#include "Light.h"
#include "Car.h"
#include "GL\glew.h"
#include "Mtx44.h"
#include "shader.hpp"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"

class P2CarSelect : public Scene
{
	enum UNIFORM_TYPE
	{
		U_MVP = 0,
		U_MODELVIEW,
		U_MODELVIEW_INVERSE_TRANSPOSE,
		U_MATERIAL_AMBIENT,
		U_MATERIAL_DIFFUSE,
		U_MATERIAL_SPECULAR,
		U_MATERIAL_SHININESS,

		U_LIGHT0_POSITION,
		U_LIGHT0_COLOR,
		U_LIGHT0_POWER,
		U_LIGHT0_KC,
		U_LIGHT0_KL,
		U_LIGHT0_KQ,

		//add these enum in UNIFORM_TYPE before U_TOTAL
		U_LIGHT0_TYPE,
		U_LIGHT0_SPOTDIRECTION,
		U_LIGHT0_COSCUTOFF,
		U_LIGHT0_COSINNER,
		U_LIGHT0_EXPONENT,

		//add these enum in UNIFORM_TYPE before U_TOTAL
		U_NUMLIGHTS, //in case you missed out practical 7
		U_LIGHTENABLED,
		U_COLOR_TEXTURE_ENABLED,
		U_COLOR_TEXTURE,
		U_TEXT_ENABLED,
		U_TEXT_COLOR,
		U_TOTAL,
	};

	enum GEOMETRY_TYPE
	{
		GEO_QUAD,
		GEO_TEXT,
		NUM_GEOMETRY,
	};
public:
	P2CarSelect();
	~P2CarSelect();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();
	virtual void setCameraType(int a);
	virtual int getMapNumber();
	virtual void setMapNumber(int number);
	virtual int getP1Car();
	virtual void setP1Car(int car1);
	virtual int getP2Car();
	virtual void setP2Car(int car2);
	MS modelStack, viewStack, projectionStack;
	int P2car1 = LoadTGA("Image//P2ChooseCar1.tga");
	int P2car2 = LoadTGA("Image//P2ChooseCar2.tga");
	int P2car3 = LoadTGA("Image//P2ChooseCar3.tga");

private:
	unsigned m_vertexArrayID;
	Mesh* meshList[NUM_GEOMETRY]; // New to Scene 3
	unsigned m_programID;
	unsigned m_parameters[U_TOTAL];
	float delaytimer;
	int FPS;
	Camera2 StartMenuCamera;
	void RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y);
	Light light[1];
	void RenderMesh(Mesh *mesh, bool enableLight);
};