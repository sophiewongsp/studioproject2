#include "Camera.h"
#include "Application.h"
#include "Mtx44.h"

Camera::Camera()
{
	Reset();
}

Camera::~Camera()
{
}

void Camera::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = pos;
	this->target = target;
	this->up = up;
}

void Camera::Init(const Vector3& pos, const Vector3& target, const Vector3& up, const Vector3& Cpos, const Vector3 Ctarget, const Vector3 Cposition)
{
	this->position = pos;
	this->target = target;
	this->up = up;
}

void Camera::Reset()
{
	position.Set(1, 0, 0);
	target.Set(0, 0, 0);
	up.Set(0, 1, 0);
}

void Camera::Update(const Vector3& Cview, const Vector3& Cposition)
{
}

void Camera::Update(double dt)
{
}

int Camera::getPositionX()
{
	return position.x;
}

int Camera::getPositionY()
{
	return position.y;
}

int Camera::getPositionZ()
{
	return position.z;
}

int Camera::getTargetX()
{
	return target.x;
}

int Camera::getTargetY()
{
	return target.y;
}

int Camera::getTargetZ()
{
	return target.z;
}

int Camera::getUpX()
{
	return up.x;
}

int Camera::getUpY()
{
	return up.y;
}

int Camera::getUpZ()
{
	return up.z;
}