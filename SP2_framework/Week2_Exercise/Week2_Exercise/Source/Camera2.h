#ifndef CAMERA_2_H
#define CAMERA_2_H

#include "Camera.h"
#include "MeshBuilder.h"

class Camera2 : public Camera
{
public:
	Camera2();
	~Camera2();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up, const Vector3& Cpos, const Vector3 Ctarget, const Vector3 Cposition);
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Update(double dt);
	virtual void Update(const Vector3& Cview, const Vector3& Cposition);
	virtual void Reset();

	void setTargetDist(const Vector3& dist);
	void setCameraAngle(float angle);

private:
	Vector3 defaultPosition;
	Vector3 defaultTarget;
	Vector3 defaultUp;
	Vector3 targetDist;
	Vector3 vectorFromCar;
	Vector3 unitVectorUp;

	float cameraAngle;
	float magnitudeAdj;
	float magnitudeFromCar;
	float angleFromCar;
};

#endif