#ifndef CAMERA_H
#define CAMERA_H

#include "Vector3.h"

class Camera
{
public:
	Camera();
	~Camera();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up, const Vector3& Cpos, const Vector3 Ctarget, const Vector3 Cposition);
	virtual void Reset();
	virtual void Update(double dt);
	virtual void Update(const Vector3& Cview, const Vector3& Cposition);

	int getPositionX();
	int getPositionY();
	int getPositionZ();
	int getTargetX();
	int getTargetY();
	int getTargetZ();
	int getUpX();
	int getUpY();
	int getUpZ();

protected:
	Vector3 position;
	Vector3 target;
	Vector3 up;
};

#endif