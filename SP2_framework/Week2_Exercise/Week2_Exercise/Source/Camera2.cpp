#include "Camera2.h"
#include "Application.h"
#include "Mtx44.h"

#define PI 3.14159265

Camera2::Camera2()
{
	targetDist.SetZero();
	vectorFromCar.SetZero();
	cameraAngle = 0;
	magnitudeFromCar = 0;
	magnitudeAdj = 0;
	angleFromCar = 0;
	unitVectorUp = Vector3(0, 1, 0);
}

Camera2::~Camera2()
{
}

void Camera2::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	right.y = 0;
	right.Normalize();
}

void Camera2::Init(const Vector3& pos, const Vector3& target, const Vector3& up, const Vector3& Cpos, const Vector3 Ctarget, const Vector3 Cposition)
{
	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	this->vectorFromCar = pos - Cpos;
	magnitudeFromCar = vectorFromCar.Length();
	Vector3 Cview = (Ctarget - Cposition).Normalized();
	this->angleFromCar = (acos((-Cview.Dot(-(view)))) * 180.0 / PI);
	magnitudeAdj = cos(angleFromCar * PI / 180.0) * magnitudeFromCar;
}

void Camera2::Update(const Vector3& Cview, const Vector3& Cposition)
{
	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	position = Cposition - Cview * this->magnitudeAdj + Vector3(0, 50, 0) + targetDist;
	target = Cposition + Vector3(0, cameraAngle, 0);
}

void Camera2::Update(double dt)
{
	static const float CAMERA_SPEED = 50.f;

	Vector3 view = (target - position).Normalized();
	Vector3 right = view.Cross(up);
	target = position + view;
}

void Camera2::Reset()
{
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}

void Camera2::setTargetDist(const Vector3& dist)
{	
	this->targetDist = dist;
}	
	
void Camera2::setCameraAngle(float angle)
{
	this->cameraAngle = angle;
}