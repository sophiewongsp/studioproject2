#include "Car.h"

Car::Car()
{
	Cposition.Set(0, 0, 1);
	Ctarget.Set(0, 0, 0);
	Cup.Set(0, 1, 0);

	yawNew = yaw = speed = bananaYAW = 0.0f;
	cooldown = 200;
	Pcooldown2 = 200;
	rotation.SetToZero();

	time = carChosen = duration = Pduration2 = bananaActivated = 0;
	// for nitro
	nitro = acceleration = turningL = turningR = reverse = reverseL = reverseR = isCollided = false; 
	//power
	hacked = magnet2 = Car2putBanana = isbanana = collideBanana = false;

	hacked = false;
	magnet2 = false;
	putBanana = false;
	isbanana = false;
	carChosen = 0;
}

Car::~Car()
{
}

void Car::init(const Vector3& cpos, const Vector3& ctarget, const Vector3& cup)
{
	this->Cposition = cpos;
	this->Ctarget = ctarget;
	Vector3 Cview = (ctarget - Cposition).Normalized();
	Vector3 Cright = Cview.Cross(cup);
	Cright.y = 0;
	Cright.Normalize();
}
	
void Car::update(double dt)
{
	CPrePosition = Cposition;
	time++;
	//time += (int)(1 * dt); //increase time

	Cview = (Ctarget - Cposition).Normalized();
	Cright = Cview.Cross(Cup);
	Ctarget = Cposition + Cview;

	float turn_speed = 100.f;


	//std::cout << "Speed: " << speed << std::endl;
	//std::cout << "Duration2: " << Pduration2 << std::endl;
	//std::cout << "CD2: " << Pcooldown2 << std::endl;
	//std::cout << "player2: " << carChosen << std::endl;
	//std::cout << "collide: " << isCollided << std::endl;
	//std::cout << "collideWall: " << isCollidedWall << std::endl;


	// Player 1
	if (time > 300) // remember to change back to 300
	{
		if (!collideBanana)
		{
			if (speed >= 0 && reverse == false) //moving forward if 'W' was pressed
			{
				Cposition += Cview * (float)(speed * dt);
			}
			else if (speed >= 0 && reverse == true) //reversing if 'S' was pressed
			{
				Cposition -= Cview * (float)(speed * dt);
			}
			if (speed > 0 && (turningL == true || reverseR == true) && isCollided == false && isCollidedWall == false) //turn left when speed above 0
			{
				yaw = (float)(turn_speed * dt);
				rotation.SetToRotation(yaw, 0, 1, 0);
				Cup = rotation * Cup;
				Cview = rotation * Cview;
			}
			else if (speed > 0 && (turningR == true || reverseL == true) && isCollided == false && isCollidedWall == false) //turn right when speed above 0
			{
				yaw = (float)(-turn_speed * dt);
				rotation.SetToRotation(yaw, 0, 1, 0);
				Cup = rotation * Cup;
				Cview = rotation * Cview;
			}
		}
		if (collideBanana && !isCollided) //if collided with banana
		{
			yaw = (float)(bananaYAW * dt);
			rotation.SetToRotation(yaw, 0, 1, 0);
			Cup = rotation * Cup;
			Cview = rotation * Cview;
			bananaYAW = 0.0f;
		}
		yawNew += yaw;
		yaw = 0.0f;
		rotation.SetToZero();
		Ctarget = Cposition + Cview;

		if (Application::IsKeyPressed('W')) //drive
		{
			acceleration = true; // diff maxspeed, nitro speed, cd & duration, power cooldown & duration
			reverse = false;
			// accelerate
			if (acceleration && !turningL && !turningR && !collideBanana)
			{
				if (speed <= maxspeed1 && !isCollided && !isCollidedWall) // maxspeed, if car1 = 100, if car2 = 80 etc
				{
					speed += (float)(20 * dt);
				}
			}
			// use nitro
			if (nitro == true)
			{
				speed = nitrospd1;
				cooldown = 0;

				if (duration <= nitrodur1 && acceleration)
				{
					duration++;
				}
			}
			if (duration == nitrodur1) //duration of nitro
			{
				nitro = false;
				speed = maxspeed1;
				duration = 0;
			}
			if (cooldown <= (nitrocd1 - 1))
			{
				cooldown++;
			}
		}
		else if (Application::IsKeyPressed('S')) //reverse
		{
			if (speed > 0 && !reverse)
			{
				speed -= (float)(100 * dt);
			}
			if (speed == 0)
			{
				acceleration = true;
				reverse = true;
			}
			if (reverse == true)
			{
				if (speed <= maxspeed1 && !collideBanana)
				{
					speed += (float)(20 * dt);
				}
			}
		}
		else //vehicle not moving
		{
			acceleration = false;
			if (acceleration == false && speed > 0)
			{
				speed -= (float)(30 * dt);
			}
		}
		

		// reversing
		if (speed > 1 && reverse == false && isCollided == false && isCollidedWall == false) // vehicle can only turn when moving forward
		{
			reverseL = false;
			reverseR = false;
			if (Application::IsKeyPressed('A')) //left
			{
				turningR = false;
				turningL = true;
			}
			else if (Application::IsKeyPressed('D')) //right
			{
				turningL = false;
				turningR = true;
			}
			else
			{
				turningL = false;
				turningR = false;
			}
		}
		if (speed > 1 && reverse == true && isCollided == false && isCollidedWall == false)
		{
			turningL = false;
			turningR = false;
			if (Application::IsKeyPressed('D'))
			{
				reverseL = false;
				reverseR = true;
			}
			else if (Application::IsKeyPressed('A'))
			{
				reverseR = false;
				reverseL = true;
			}
			else
			{
				reverseL = false;
				reverseR = false;
			}
		}

		if (Application::IsKeyPressed('Q')) //brake
		{
			acceleration = false;
			if (acceleration == false)
			{
				if (speed > 0)
				{
					speed -= (float)(50 * dt);
				}
			}
		}

		if (Application::IsKeyPressed(VK_OEM_PERIOD))//power
		{
			if (carChosen == 1)//power
			{
				if (Pcooldown2 == powercd2)
				{
					magnet2 = true;
				}
			}
			else if (carChosen == 2)
			{
				if ((Pcooldown2) == powercd2)
				{
					hacked = true;
				}
			}
			else
			{
				if (Pcooldown2 == powercd2)//banana
				{
					Pcooldown2 = 0;
					Car2putBanana = true;
				}
			}
		}

		// using magnet
		if (magnet2 == true)
		{
			Pcooldown2 = 0;
			if (speed > 30)
			{
				speed -= (float)(100 * dt);
			}
			if (Pduration2 <= (powerdur2 - 1))
			{
				Pduration2++;
			}
		}
		//using hack
		if (hacked == true)
		{
			Pcooldown2 = 0;
			if (Pduration2 <= (powerdur2 - 1))
			{
				Pduration2++;
			}
		}
		if (Pduration2 == powerdur2)
		{
			magnet2 = false;
			hacked = false;
			Pduration2 = 0;
		}
		if (Pcooldown2 <= (powercd2 - 1))
		{
			Pcooldown2++;
		}

		if (Application::IsKeyPressed(VK_SPACE) && cooldown == nitrocd1) //nitro
		{
			nitro = true;
		}
		if (speed < 0) // ensure speed does not go below 0
		{
			speed = 0;
		}
		if (isCollided || isCollidedWall) // if cars collide, speed = 0
		{
			speed = 0;
			turn_speed = 0;

		}
	}
}

float Car::getrotateWheels()
{
	return rotateWheels;
}

float Car::getAngle()
{
	return yawNew;
}

float Car::getCposx()
{
	return Cposition.x;
}

float Car::getCposy()
{
	return Cposition.y;
}

float Car::getCposz()
{
	return Cposition.z;
}

float Car::getActiveBanana()
{
	return bananaActivated;
}

int Car::getTime()
{
	return time;
}

Vector3 Car::getCpos()
{
	return Cposition;
}

Vector3 Car::getCtarget()
{
	return Ctarget;
}

Vector3 Car::getCview()
{
	return Cview;
}

Vector3 Car::getCup()
{
	return Cup;
}

Vector3 Car::getCright()
{
	return Cright;
}

Vector3 Car::getPrePos()
{
	return CPrePosition;
}

void Car::setAngle(float YAWWWWWWWWWW, double dt)
{
	bananaYAW = YAWWWWWWWWWW;
	//bananaYAW += (float)(YAWWWWWWWWWW * dt);
}

void Car::setSpeed(float spd, double dt)
{
	speed += (float)(-spd * dt);
}

void Car::setActiveBanana(float TimerBanana)
{
	bananaActivated = TimerBanana;
}

void Car::CalBanana(float TimerBanana, double dt)
{
	bananaActivated += (float)(TimerBanana * dt);
}

void Car::setCposition(Vector3 getPrePos)
{
	Cposition = getPrePos;
}

void Car::setCollided(bool Collided)
{
	isCollided = Collided;
}

void Car::setCollidedWall(bool Collided)
{
	isCollidedWall = Collided;
}

void Car::setSpeedTo0()
{
	speed = 0;
}

bool Car::getHacked()
{
	return hacked;
}

float Car::getSpeed()
{
	return speed;
}

int Car::getpowercd2()
{
	return powercd2;
}

int Car::getPcooldown2()
{
	return Pcooldown2;
}

int Car::getCooldown()
{
	return cooldown;
}