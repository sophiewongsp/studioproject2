//Include GLEW
#include <GL/glew.h>

//Include GLFW
#include <GLFW/glfw3.h>

//Include the standard C++ headers
#include <stdio.h>
#include <stdlib.h>

#include "Application.h"

#include "Scene1.h"
#include "StartMenu.h"
#include "Instructions.h"
#include "MapSelect.h"
#include "P1CarSelect.h"
#include "P2CarSelect.h"

GLFWwindow* m_window;
const unsigned char FPS = 60; // FPS of this game
const unsigned int frameTime = 1000 / FPS; // time for each frame

//Define an error callback
static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

//Define the key input callback
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

bool Application::IsKeyPressed(unsigned short key)
{
    return ((GetAsyncKeyState(key) & 0x8001) != 0);
}

Application::Application()
{
}

Application::~Application()
{
}

void resize_callback(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, w, h); //update opengl the new window size
}

void Application::Init()
{
	//Set the error callback
	glfwSetErrorCallback(error_callback);

	//Initialize GLFW
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Set the GLFW window creation hints - these are optional
	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Request a specific OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //Request a specific OpenGL version
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 
	


	//Create a window and create its OpenGL context
	m_window = glfwCreateWindow(800, 600, "Test Window", NULL, NULL);
	glfwSetWindowSizeCallback(m_window, resize_callback);

	//If the window couldn't be created
	if (!m_window)
	{
		fprintf( stderr, "Failed to open GLFW window.\n" );
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	//This function makes the context of the specified window current on the calling thread. 
	glfwMakeContextCurrent(m_window);

	//Sets the key callback
	//glfwSetKeyCallback(m_window, key_callback);

	glewExperimental = true; // Needed for core profile
	//Initialize GLEW
	GLenum err = glewInit();

	//If GLEW hasn't initialized
	if (err != GLEW_OK) 
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		//return -1;
	}
}

void Application::Run()
{
	//Main Loop
	Scene *startMenu = new StartMenu();
	startMenu->Init();
	m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame
	while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE) && !IsKeyPressed(VK_SPACE))
	{
		hello:
		startMenu->Update(m_timer.getElapsedTime());
		startMenu->Render();
		//Swap buffers
		glfwSwapBuffers(m_window);
		//Get and organize events, like keyboard and mouse input, window resizing, etc...
		glfwPollEvents();
		m_timer.waitUntil(frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.   

		if (IsKeyPressed(VK_SPACE) && startMenu->PlayHL)
		{
			Scene *mapselect = new MapSelect();
			mapselect->Init();
			m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame
			while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE))
			{
				mapselect->Update(m_timer.getElapsedTime());
				mapselect->Render();
				//Swap buffers
				glfwSwapBuffers(m_window);
				//Get and organize events, like keyboard and mouse input, window resizing, etc...
				glfwPollEvents();
				m_timer.waitUntil(frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.   

				if (IsKeyPressed(VK_SPACE))
				{
					Scene *P1carselect = new P1CarSelect();
					P1carselect->Init();
					m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame
					while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE))
					{
						P1carselect->Update(m_timer.getElapsedTime());
						P1carselect->Render();
						//Swap buffers
						glfwSwapBuffers(m_window);
						//Get and organize events, like keyboard and mouse input, window resizing, etc...
						glfwPollEvents();
						m_timer.waitUntil(frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.   

						if (IsKeyPressed(VK_SPACE))
						{
							Scene *P2carselect = new P2CarSelect();
							P2carselect->Init();
							m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame
							while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE))
							{
								P2carselect->Update(m_timer.getElapsedTime());
								P2carselect->Render();
								//Swap buffers
								glfwSwapBuffers(m_window);
								//Get and organize events, like keyboard and mouse input, window resizing, etc...
								glfwPollEvents();
								m_timer.waitUntil(frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.   
								if (IsKeyPressed(VK_SPACE))
								{
									Scene *scene = new Scene1();
									scene->setMapNumber(mapselect->getMapNumber());
									scene->setP1Car(P1carselect->getP1Car());
									scene->setP2Car(P2carselect->getP2Car());
									scene->Init();
									m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame
									while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE))
									{
										scene->Update(m_timer.getElapsedTime());

										glEnable(GL_SCISSOR_TEST);

										glfwGetWindowSize(m_window, &windowWidth, &windowHeight);

										// up down view
										glScissor(0, windowHeight / 2, windowWidth, windowHeight / 2);
										scene->setCameraType(1);
										scene->Render();

										glScissor(0, 0, windowWidth, windowHeight / 2);
										scene->setCameraType(2);
										scene->Render();

										//first person view
										if (Application::IsKeyPressed(VK_LSHIFT))
										{
											glScissor(0, windowHeight / 2, windowWidth, windowHeight / 2);
											scene->setCameraType(3);
											scene->Render();
										}

										if (Application::IsKeyPressed(VK_RSHIFT))
										{
											glScissor(0, 0, windowWidth, windowHeight / 2);
											scene->setCameraType(4);
											scene->Render();
										}

										if (scene->car1lap >= 3 && scene->car2lap >= 3)
										{
											glScissor(0, 0, windowWidth, windowHeight);
											scene->setCameraType(6);
											scene->Render();
										}

										//mini map
										/*glViewport(0, windowHeight / 3, windowHeight / 3, windowHeight / 3);
										scene->setCameraType(5);
										scene->Render();

										glViewport(0, 0, 800, 600);*/
										//Swap buffers
										glfwSwapBuffers(m_window);
										//Get and organize events, like keyboard and mouse input, window resizing, etc...
										glfwPollEvents();
										m_timer.waitUntil(frameTime); // Frame rate limiter. Limits each frame to a specified time in ms.   

									} //Check if the ESC key had been pressed or if the window had been closed
									scene->Exit();
									delete scene;
								}
							} //Check if the ESC key had been pressed or if the window had been closed
							P2carselect->Exit();
							delete P2carselect;
						}
					} //Check if the ESC key had been pressed or if the window had been closed
					P1carselect->Exit();
					delete P1carselect;
				}
			} //Check if the ESC key had been pressed or if the window had been closed
			mapselect->Exit();
			delete mapselect;
		}

		if (IsKeyPressed(VK_SPACE) && !startMenu->PlayHL)
		{
			Scene *scene2 = new Instructions();
			scene2->Init();
			m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame
			while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE))
			{
				scene2->Update(m_timer.getElapsedTime());
				scene2->Render();
				//Swap buffers
				glfwSwapBuffers(m_window);
				//Get and organize events, like keyboard and mouse input, window resizing, etc...
				glfwPollEvents();
				m_timer.waitUntil(frameTime); // Frame rate limiter. Limits each frame to a specified time in ms.   

				if (IsKeyPressed(VK_SHIFT))
				{
					goto hello;
				}

			} //Check if the ESC key had been pressed or if the window had been closed
			scene2->Exit();
			delete scene2;
		}

	} //Check if the ESC key had been pressed or if the window had been closed
	startMenu->Exit();
	delete startMenu;

	
}

void Application::Exit()
{
	//Close OpenGL window and terminate GLFW
	glfwDestroyWindow(m_window);
	//Finalize and clean up GLFW
	glfwTerminate();
}
