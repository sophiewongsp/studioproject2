#include "CheckCollision.h"



CheckCollision::CheckCollision()
{
	CarSize.Set(2.6f, 0, 6);
	BananaSize.Set(2,0,2);
	highest = lowest = 0.0f;
}


CheckCollision::~CheckCollision()
{
}

void CheckCollision::CheckWalls(Vector3 norm)
{
	this->lowest = HUGE;//set it to highest to find lowest dot product
	this->highest = -HUGE;//set it to lowest to find highest dot product

	float dotVal;
	for (int i = 0; i < 4; i++)
	{
		dotVal = this->Pt[i].Dot(norm);//find the dot of 2 cars and get the lowest and highest and use to check car to car collision
		if (dotVal < this->lowest)
			this->lowest = dotVal;
		if (dotVal > this->highest)
			this->highest = dotVal;
	}
}

void CheckCollision::CreateLapCol(Vector3 & normal)
{

	LapLowest = (float)HUGE;
	LapHighest = (float)-HUGE;
	float dotVal;
	for (int i = 0; i < 4; i++)
	{
		dotVal = lapPoints[i].Dot(normal);//find the dot of all cars and laps
		if (dotVal < LapLowest)
			LapLowest = dotVal;
		if (dotVal > LapHighest)
			LapHighest = dotVal;
	}
}

void CheckCollision::CreateBananaCol(Vector3& normal, const Vector3 & bananaPos)
{
	Pt[0] = bananaPos + BananaSize;
	Pt[1] = bananaPos + (BananaSize + (-BananaSize.x * 2));
	Pt[2] = bananaPos - (BananaSize - (BananaSize.x * 2));
	Pt[3] = bananaPos - BananaSize;

	BLowest = (float)HUGE;
	BHighest = (float)-HUGE;
	float dotVal;
	for (int i = 0; i < 4; i++)
	{
		dotVal = Pt[i].Dot(normal);//find the dot of all cars and banana
		if (dotVal < BLowest)
			BLowest = dotVal;
		if (dotVal > BHighest)
			BHighest = dotVal;
	}
}

void CheckCollision::CreateCarCol(Vector3& normal, const Vector3& Cposition, float yawNew)
{
	rotation.SetToRotation(yawNew, 0, 1, 0);

	Pt[0] = Cposition + rotation * CarSize;						//car front left (2.6f, 0, 6)
	Pt[1] = Cposition + rotation * (CarSize + (-CarSize.x * 2)); //car front right (-2.6f, 0, 6)
	Pt[2] = Cposition - rotation * (CarSize - (CarSize.x * 2)); //car back left (2.6f, 0, -7)
	Pt[3] = Cposition - rotation * CarSize;						// car back right (-2.6f, 0, -7)

	lowest = (float)HUGE; 
	highest = (float)-HUGE;
	float dotVal;
	for (int i = 0; i < 4; i++)
	{
		dotVal = this->Pt[i].Dot(normal);//find the dot of 2 cars and get the lowest and highest and use to check car to car collision
		if (dotVal < this->lowest)
			this->lowest = dotVal;
		if (dotVal > this->highest)
			this->highest = dotVal;
	}
}

float CheckCollision::getHighest()
{
	return highest;
}

float CheckCollision::getLowest()
{
	return lowest;
}