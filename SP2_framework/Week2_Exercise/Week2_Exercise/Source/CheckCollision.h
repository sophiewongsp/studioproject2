#ifndef CHECKCOLLISION_H
#define CHECKCOLLISION_H

#include <vector>
#include "Vector3.h"
#include "Mtx44.h"


class CheckCollision
{
private:

	Vector3 CarSize;
	Vector3 BananaSize;
	Mtx44 rotation;
	float lowest, highest;


public:
	CheckCollision();
	~CheckCollision();

	float BLowest, BHighest;
	float LapLowest, LapHighest;
	Vector3 lapsNormal[2];
	Vector3 Pt[4];
	Vector3 Bt[4];
	Vector3 lapPoints[4];
	float innerLow_CartoWall, innerHigh_CartoWall;
	float innerLow_WalltoCar, innerHigh_WalltoCar;
	float innerLow[8], innerHigh[8];

	float outerLowCar, outerHighCar;
	float outerLow, outerHigh;

	Vector3 innerWall[8];
	Vector3 innerNorm[8];
	Vector3 innerPoint_Map1[8];

	Vector3 outerWall[8];
	Vector3 outerNorm[8];
	Vector3 outerPoint_Map1[8];

	void CheckWalls(Vector3 norm);
	void CreateLapCol(Vector3& carNormals);
	void CreateBananaCol(Vector3& normal, const Vector3& bananaPos);
	void CreateCarCol(Vector3& normal, const Vector3& Cposition, float yawNew);
	float getHighest();
	float getLowest();


};

#endif // !CHECKCOLLISION_H