#ifndef CAR2_H
#define CAR2_H

#include "Application.h"
#include "Mtx44.h"
#include "Vector3.h"
#include "CheckCollision.h"

class Car2
{
private:

	float speed2, yawNew2, rotateWheels2, bananaActivated, yaw, bananaYaw2;
	// for duration of nitro, cooldown of skills, and duration of skills
	int time2, cooldown2, duration2, Pcooldown, Pduration;
	// for nitro. check if turning. check if collided
	bool nitro2, acceleration2, turningL2, turningR2, reverse2, reverseL2, reverseR2, isCollided2, isCollidedWall2;
	Vector3 Cposition2, Ctarget2, Cup2, Cview2, Cright2, CPrePosition2;
	Mtx44 rotation;

public:
	Car2();
	~Car2();
	CheckCollision Collision2;

	//power
	bool magnet, hacked, Car1putBanana, isbanana2, collideBanana2;
	int maxspeed2, nitrospd2, nitrodur2, nitrocd2, powerdur1, powercd1;
	int carChosen;
	//float tempLockYaw;

	virtual void init(const Vector3& cpos, const Vector3& ctarget, const Vector3& cup);
	virtual void update(double dt);
	float getrotateWheels2();
	float getAngle2();
	float getCposx2();
	float getCposy2();
	float getCposz2();
	float getActiveBanana();
	int getTime2();
	Vector3 getCpos2();
	Vector3 getCtarget2();
	Vector3 getCview2();
	Vector3 getCup2();
	Vector3 getCright2();
	Vector3 getPrePos2();

	void setAngle2(float YAWWWWWWWWWW, double dt);
	void setSpeed2(float spd, double dt);
	void setActiveBanana(float TimerBanana);
	void CalBanana2(float TimerBanana, double dt);
	void setCposition2(Vector3 getPrePos);
	void setCollided2(bool Collided);
	void setCollidedWall2(bool Collided);
	void setSpeedTo0();
	float getSpeed2();
	int getpowercd1();
	int getPcooldown();
	int getCooldown2();
	bool getHacked();
};

#endif