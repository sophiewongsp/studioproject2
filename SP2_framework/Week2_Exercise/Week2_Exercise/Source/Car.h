#ifndef CAR_H
#define CAR_H

#include "Application.h"
#include "Mtx44.h"
#include "Vector3.h"
#include "CheckCollision.h"

class Car
{
private:

	float yawNew, rotateWheels, speed, bananaActivated, yaw, bananaYAW;
	int time, cooldown, Pcooldown2, duration, Pduration2;
	bool nitro, acceleration, turningL, turningR, reverse, reverseL, reverseR; // for nitro
	Vector3 Ctarget, Cposition, Cup, Cview, Cright, CPrePosition;
	Mtx44 rotation;
public:
	Car();
	~Car();
	CheckCollision Collision;
	
	//power & nitro
	bool magnet2, hacked, Car2putBanana, isbanana, collideBanana, putBanana, isCollided, isCollidedWall;
	int maxspeed1, nitrospd1, nitrodur1, nitrocd1, powerdur2, powercd2;
	int carChosen;

	//float tempLockYaw;

	virtual void init(const Vector3& cpos, const Vector3& ctarget, const Vector3& cup);
	virtual void update(double dt);
	float getrotateWheels();
	float getAngle();
	float getCposx();
	float getCposy();
	float getCposz();
	float getActiveBanana();
	int getTime();
	Vector3 getCpos();
	Vector3 getCtarget();
	Vector3 getCview();
	Vector3 getCup();
	Vector3 getCright();
	Vector3 getPrePos();

	void setAngle(float YAWWWWWWWWWW, double dt);
	void setSpeed(float spd, double dt);
	void setActiveBanana(float TimerBanana);
	void CalBanana(float TimerBanana, double dt);
	void setCposition(Vector3 getPrePos);
	void setCollided(bool Collided);
	void setCollidedWall(bool Collided);
	void setSpeedTo0();
	float getSpeed();
	int getpowercd2();
	int getPcooldown2();
	int getCooldown();
	bool getHacked();
};

#endif