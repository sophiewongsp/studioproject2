#include "Car2.h"

Car2::Car2()
{
	Cposition2.Set(0, 0, 1);
	Ctarget2.Set(0, 0, 0);
	Cup2.Set(0, 1, 0);
	
	yawNew2 = yaw = speed2 = bananaYaw2 = 0.0f;
	cooldown2 = 200;
	Pcooldown = 200;
	rotation.SetToZero();

	//
	time2 = carChosen = duration2 = Pduration = bananaActivated = 0;
	// for nitro, speed, turning and collide to false
	nitro2 = acceleration2 = turningL2 = turningR2 = reverse2 = reverseL2 = reverseR2 = isCollided2 = isCollidedWall2 = false;
	//powers
	hacked = magnet = Car1putBanana = isbanana2 = collideBanana2 = false;
}

Car2::~Car2()
{
}

void Car2::init(const Vector3& cpos, const Vector3& ctarget, const Vector3& cup)
{
	this->Cposition2 = cpos;
	this->Ctarget2 = ctarget;
	Vector3 Cview = (ctarget - Cposition2).Normalized();
	Vector3 Cright = Cview.Cross(cup);
	Cright.y = 0;
	Cright.Normalize();
}

void Car2::update(double dt)
{
	CPrePosition2 = Cposition2;
	time2++;//increase time

	Cview2 = (Ctarget2 - Cposition2).Normalized();
	Cright2 = Cview2.Cross(Cup2);
	Ctarget2 = Cposition2 + Cview2;

	float turn_speed = 100.f;

	//std::cout << Cview2 << std::endl;
	//std::cout << Cposition2.z << std::endl;
	//std::cout << "Speed2: " << speed2 << std::endl;
	//std::cout << "P-Duration: " << Pduration << std::endl;
	//std::cout << "P-CD: " << Pcooldown << std::endl;
	//std::cout << "player1: " << carChosen << std::endl;

	// Player 2
	if (time2 > 300) // remember to change back to 300
	{
		if (!collideBanana2)
		{
			if (speed2 >= 0 && reverse2 == false)
			{
				Cposition2 += Cview2 * (float)(speed2 * dt);
			}
			else if (speed2 >= 0 && reverse2 == true)
			{
				Cposition2 -= Cview2 * (float)(speed2 * dt);
			}
			if (speed2 > 0 && (turningL2 == true || reverseR2 == true) && isCollided2 == false && isCollidedWall2 == false) //turn left when speed above 0
			{
				yaw = (float)(turn_speed * dt);
				rotation.SetToRotation(yaw, 0, 1, 0);
				Cup2 = rotation * Cup2;
				Cview2 = rotation * Cview2;

			}
			else if (speed2 > 0 && (turningR2 == true || reverseL2 == true) && isCollided2 == false && isCollidedWall2 == false) //turn right when speed above 0
			{
				yaw = (float)(-turn_speed * dt);
				rotation.SetToRotation(yaw, 0, 1, 0);
				Cup2 = rotation * Cup2;
				Cview2 = rotation * Cview2;
			}
		}
			if (collideBanana2 && !isCollided2) //if collided with banana
			{
				yaw = (float)(bananaYaw2 * dt);
				rotation.SetToRotation(yaw, 0, 1, 0);
				Cup2 = rotation * Cup2;
				Cview2 = rotation * Cview2;
				bananaYaw2 = 0.0f;
			}
			yawNew2 += yaw;
			yaw = 0.0f;
			rotation.SetToZero();
			Ctarget2 = Cposition2 + Cview2;

			if (Application::IsKeyPressed(VK_UP)) //drive
			{
				acceleration2 = true;
				reverse2 = false;

				// accelerate
				if (acceleration2 && !turningL2 && !turningR2 && !collideBanana2)
				{
					if (speed2 <= maxspeed2 && !isCollided2 && !isCollidedWall2)
					{
						speed2 += (float)(20 * dt);
					}
				}
				// use nitro
				if (nitro2 == true)
				{
					speed2 = nitrospd2;
					cooldown2 = 0;

					if (duration2 <= nitrodur2 && acceleration2)
					{
						duration2++;
					}
				}
				if (duration2 == nitrodur2)
				{
					nitro2 = false;
					speed2 = maxspeed2;
					duration2 = 0;
				}
				if (cooldown2 <= (nitrocd2 - 1))
				{
					cooldown2++;
				}

			}
			else if (Application::IsKeyPressed(VK_DOWN)) //reverse
			{
				if (speed2 > 0 && !reverse2)
				{
					speed2 -= (float)(100 * dt);
				}
				if (speed2 == 0)
				{
					acceleration2 = true;
					reverse2 = true;
				}
				if (reverse2 == true)
				{
					if (speed2 <= maxspeed2 && !collideBanana2)
					{
						speed2 += (float)(20 * dt);
					}
				}
			}
			else //vehicle not moving
			{
				acceleration2 = false;
				if (acceleration2 == false)
				{
					if (speed2 > 0)
						speed2 -= (float)(30 * dt);
				}
			}
		


		// reversing
		if (speed2 > 1 && reverse2 == false && isCollided2 == false && isCollidedWall2 == false) // vehicle can only turn when moving forward
		{
			reverseL2 = false;
			reverseR2 = false;
			if (Application::IsKeyPressed(VK_LEFT)) //left
			{
				turningR2 = false;
				turningL2 = true;
			}
			else if (Application::IsKeyPressed(VK_RIGHT)) //right
			{
				turningL2 = false;
				turningR2 = true;
			}
			else
			{
				turningL2 = false;
				turningR2 = false;
			}
		}
		if (speed2 > 1 && reverse2 == true && isCollided2 == false && isCollidedWall2 == false)
		{
			turningL2 = false;
			turningR2 = false;
			if (Application::IsKeyPressed(VK_RIGHT))
			{
				reverseL2 = false;
				reverseR2 = true;
			}
			else if (Application::IsKeyPressed(VK_LEFT))
			{
				reverseR2 = false;
				reverseL2 = true;
			}
			else
			{
				reverseL2 = false;
				reverseR2 = false;
			}
		}

		if (Application::IsKeyPressed(VK_OEM_2)) //brake
		{
			acceleration2 = false;

			if (speed2 > 0)
				speed2 -= (float)(50 * dt);
		}

		if (Application::IsKeyPressed('F')) //power
		{
			if (carChosen == 1)
			{
				if (Pcooldown == powercd1)
				{
					magnet = true;
				}
			}
			else if (carChosen == 2)
			{
				if (Pcooldown == powercd1)
				{
					hacked = true;
				}
			}
			else
			{
				if (Pcooldown == powercd1)//banana
				{
					Pcooldown = 0;
					Car1putBanana = true;
				}
			}
		}

		// using magnet
		if (magnet == true)
		{
			Pcooldown = 0;
			if (speed2 > 30)
			{
				speed2 -= (float)(100 * dt);
			}
			if (Pduration <= (powerdur1 - 1))
			{
				Pduration++;
			}
		}
		// using hack
		if (hacked == true)
		{
			Pcooldown = 0;
			if (Pduration <= (powerdur1 - 1))
			{
				Pduration++;
			}
		}
		if (Pduration == powerdur1)
		{
			magnet = false;
			hacked = false;
			Pduration = 0;
		}
		if (Pcooldown <= (powercd1 - 1))
		{
			Pcooldown++;
		}

		if (Application::IsKeyPressed(VK_RCONTROL) && cooldown2 == nitrocd2) //nitro
		{
			nitro2 = true;
		}
		if (speed2 < 0) // ensure speed does not go below 0
		{
			speed2 = 0;
		}
		if (isCollided2 || isCollidedWall2) // if cars collide, speed = 0
		{
			speed2 = 0;
			turn_speed = 0;
		}
	}
}

float Car2::getrotateWheels2()
{
	return rotateWheels2;
}

float Car2::getAngle2()
{
	return yawNew2;
}

float Car2::getCposx2()
{
	return Cposition2.x;
}

float Car2::getCposy2()
{
	return Cposition2.y;
}

float Car2::getCposz2()
{
	return Cposition2.z;
}

float Car2::getActiveBanana()
{
	return bananaActivated;
}

int Car2::getTime2()
{
	return time2;
}

Vector3 Car2::getCpos2()
{
	return Cposition2;
}

Vector3 Car2::getCtarget2()
{
	return Ctarget2;
}

Vector3 Car2::getCview2()
{
	return Cview2;
}

Vector3 Car2::getCup2()
{
	return Cup2;
}

Vector3 Car2::getCright2()
{
	return Cright2;
}

Vector3 Car2::getPrePos2()
{
	return CPrePosition2;
}


void Car2::setAngle2(float YAWWWWWWWWWW, double dt)
{
	bananaYaw2 = YAWWWWWWWWWW;
}

void Car2::setSpeed2(float spd, double dt)
{
	speed2 += (float)(-spd * dt);
}

void Car2::setActiveBanana(float TimerBanana)
{
	bananaActivated = TimerBanana;
}

void Car2::CalBanana2(float TimerBanana, double dt)
{
	bananaActivated += (float)(TimerBanana * dt);
}

void Car2::setCposition2(Vector3 getPrePos)
{
	Cposition2 = getPrePos;
}

void Car2::setCollided2(bool Collided)
{
	isCollided2 = Collided;
}

void Car2::setCollidedWall2(bool Collided)
{
	isCollidedWall2 = Collided;
}

void Car2::setSpeedTo0()
{
	speed2 = 0;
}

bool Car2::getHacked()
{
	return hacked;
}

float Car2::getSpeed2()
{
	return speed2;
}

int Car2::getpowercd1()
{
	return powercd1;
}

int Car2::getPcooldown()
{
	return Pcooldown;
}

int Car2::getCooldown2()
{
	return cooldown2;
}